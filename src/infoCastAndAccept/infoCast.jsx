import React, {useState} from 'react'
import { Link } from 'react-router-dom';
import './infoCast.css'
import './acceptCast.css'
import './headerInfoCastAndAccept.css'
import FooterBottom from './../home/footerBottom';

const InfoCast = () => {
    const [accept, setAccept] = useState(false)

    const ActiveAccept = () => {
        setAccept(!accept)
        console.log('abc')
    }

    return (
        <>
            <div className="header">
                <div className="header__wrapper p-lr-90">
                    <nav className="header__navbar flex-start">
                        <div >
                            <Link to= "/" className="sign-in__logo header__logo-link">
                                <svg 
                                    version="1.1" viewBox="0 0 244 50"
                                    className=""
                                    style = {{width:'244px'}}
                                >
                                    <path 
                                        pid="0" 
                                        d="M25.093 0c13.781.06 24.94 11.317 24.882 25.106C49.917 38.894 38.663 50.058 24.88 50 11.1 49.942-.059 38.683.001 24.894.057 11.106 11.31-.058 25.092 0zm11.801 31.9L14.398 16.053c.241-.26.48-.518.74-.777 2.7-2.687 5.971-4.031 9.775-4.015 3.804.015 7.064 1.388 9.741 4.098.238.241.476.482.694.743l-5.951 4.133 2.381 1.688 5.153-3.576v.02L39.33 16.7c-.692-1.203-1.584-2.325-2.616-3.39-3.231-3.292-7.167-4.947-11.788-4.967-4.6-.019-8.53 1.603-11.809 4.867a17.806 17.806 0 00-2.682 3.408l1.429 1.004 23.429 16.51c-.24.26-.48.518-.74.777-2.7 2.687-5.971 4.011-9.775 3.996-3.803-.016-7.063-1.37-9.74-4.08-.258-.26-.496-.521-.734-.782l6.111-4.251-.139-.181-2.183-1.528-5.373 3.735v-.021l-2.377 1.65a17.824 17.824 0 002.654 3.43c3.253 3.291 7.169 4.946 11.769 4.965 4.62.021 8.549-1.602 11.83-4.866 1.06-1.075 1.96-2.21 2.682-3.406l-2.383-1.67zm45.839 2.652l-12.45-.05.096-21.789a.957.957 0 00-.965-.945l-.678-.003a.96.96 0 00-.974.939v.047c-.048.095-.05.236-.05.33l-.098 22.636a1.337 1.337 0 001.351 1.326l13.805.056a.958.958 0 00.973-.938l.003-.662a1.037 1.037 0 00-1.013-.946v-.001zm27.64-22.67l-.871-.003c-.563-.003-1.027.42-1.027.937l-.075 15.878c-.014 3.157-3.252 5.735-7.197 5.72-3.943-.014-7.158-2.618-7.144-5.775l.123-15.5.052-.282a.957.957 0 00-.201-.756 1.128 1.128 0 00-.767-.332l-.87-.004c-.513-.002-.924.326-1.028.75a.857.857 0 00-.156.47l-.073 15.69c-.02 4.616 4.47 8.402 9.95 8.424 5.48.022 10.004-3.73 10.025-8.3l.122-15.5.054-.282a.952.952 0 00-.203-.755c-.152-.189-.458-.379-.714-.38zm48.638 11.226c-3.275-1.249-6.696-2.496-6.688-4.393.01-2.42 2.721-4.401 6.05-4.387 3.331.014 6.025 2.017 6.014 4.436l-.002.38a.96.96 0 00.961.953l.675.003a.96.96 0 00.971-.945v-.38c.009-1.66-.709-3.276-2.006-4.515-1.635-1.618-4.092-2.532-6.65-2.494-4.873-.02-8.698 3.047-8.715 6.938.03 3.748 4.416 5.38 8.656 6.962 3.324 1.247 6.795 2.495 6.785 4.487-.01 2.372-3.155 4.399-6.775 4.385-3.668-.016-6.747-2.07-6.737-4.44l.001-.38a.961.961 0 00-.96-.954l-.676-.002a.96.96 0 00-.97.945v.38c-.018 3.843 4.215 6.991 9.38 7.012 5.163.022 9.423-3.092 9.44-6.934-.033-3.796-4.464-5.427-8.754-7.057zM193.66 11.84l-18.844-.076c-.547-.003-.996.42-.999.94l-.002.66c-.002.52.443.946.99.949l8.103.032-.095 21.802c-.003.52.443.947.99.949l.696.002c.546.003.996-.42 1-.94l.095-21.802 8.055.031c.547.003.996-.42.999-.94l.003-.66c.003-.52-.443-.946-.99-.947zm49.264.515c-.144-.33-.481-.52-.867-.52l-.87-.005a.963.963 0 00-.677.28l-.145.14-.147.142-6.609 9.677-6.72-9.734c-.048-.046-.096-.094-.096-.14l-.143-.143a.97.97 0 00-.675-.285l-.774-.003a1.021 1.021 0 00-.872.515 1.03 1.03 0 00.045.988l7.966 11.576-.049 11.307c.006.527.437.95.964.946l.675.002a.957.957 0 00.97-.937l.049-11.308 7.923-11.509c.194-.329.243-.705.052-.989zm-37.898 12.047l3.75-7.994 3.728 8.023-7.478-.03zm15.615 10.954v-.047l-10.5-22.733c-.15-.425-.549-.71-1.102-.807-.603-.049-1.156.231-1.41.703v.046l-10.705 22.697v.047l-.204.471c-.095.283-.06.595.097.849.188.258.483.416.802.428l.854.004c.453.001.806-.282.958-.657l.05-.141 4.314-9.277 9.95.04 4.28 9.31.049.14c.15.38.5.664.952.667l.854.002c.352.002.654-.139.806-.422.152-.281.254-.563.104-.847l-.149-.473zm-86.008-15.15l.817.003a.963.963 0 00.729-.347l4.755-6.218.412-.478a.763.763 0 00.095-.915c-.135-.306-.453-.482-.816-.483l-.863-.004a.929.929 0 00-.868.608l-4.983 6.522a.94.94 0 00-.096.915c.135.22.453.394.816.397h.002zm-6.998 8.45l-.819-.002a.96.96 0 00-.728.347l-4.755 6.207-.413.48a.77.77 0 00-.094.92c.135.309.452.485.816.488l.864.002a.91.91 0 00.82-.478l5.03-6.601a.949.949 0 00.095-.92 1.027 1.027 0 00-.816-.443zm8.019-.453l.148.14 5.55 7.248c.248.284.246.662.095.99-.15.332-.5.519-.9.517l-.896-.004a1.013 1.013 0 01-.697-.286l-.149-.14c-.03-.002-.042-.02-.059-.044a.212.212 0 00-.04-.052l-5.501-7.151a1.005 1.005 0 01-.15-.237l-11.694-15.39-.446-.521c-.248-.285-.246-.662-.096-.993.15-.33.501-.518.9-.516l.897.004a.99.99 0 01.845.428l.15.19 11.992 15.721s.05.048.05.096z">                                        
                                    </path>
                                </svg>                                
                            </Link>
                        </div>

                        <div className="booking-info-header">
                            <div className="booking">
                                <span className= {accept ? "" : "hover-color"}>1. Thông tin đặt chỗ</span>
                                <span>
                                    <i className="fas fa-chevron-right"></i>
                                </span>
                                <span className={accept ? "hover-color" : ""}>2. Xác nhận thanh toán</span>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
            <div className={accept ?  "dis-none" : "page-checkout"} style={{marginTop: '79px', padding: '45px 90px', backgroundColor: '#f4f4f4'}}>
                <div className="check-out-left-right" style={{display: 'flex'}}>
                    <div className="check-out__info-left l-6">
                        <div className="mb--42">
                            <div className="login-instruction py--24 px--18">
                                <div className="row-login__instruction">
                                    <h2 className="mb--6 row-login__instruction-label">Đăng nhập và tận hưởng quyền lợi của Thành viên!</h2>
                                    <p className="mb--12 row-login__instruction-desc">Đăng ký thành viên Luxstay, trải nghiệm đột phá - Đặt phòng nhanh hơn, ưu đãi nhiều hơn nữa.</p>
                                    <Link style={{textDecoration: 'none'}}>
                                        <div className="login-instruction__btn mt--18">
                                            <span>Đăng nhập ngay</span>
                                        </div>
                                    </Link>
                                </div>
                                
                            </div>
                        </div>
                        
                        <div className="check-out-left__body">
                            <div className="check-out-left__title mt-0">
                                <h3>Thông tin đặt chỗ</h3>
                                <div className="check-out-left__input-group">
                                    <div className="check-out-left__sub-title">
                                        <span className="text-danger">* </span>
                                        <b>Số khách</b>
                                        <div className="l-6 col bgr-white mt--8">
                                            <div className="l-12 py--12 px--18">1 Khách</div>
                                        </div>
                                    </div>
                                </div>
                                <div className="check-out-left__input-group">
                                    <div className="check-out-left__sub-title">
                                        <b>2 đêm tại The Galaxy Home - 1 Phòng Ngủ, 60m2, View Thành Phố, Ban Công - Dịch Vọng</b>
                                        <div className="is-flex mt--8">
                                            <div className="l-6 col bgr-white mlr--8 bd-radius-5">
                                                <div className="l-12 p--12">
                                                    <hr className="is-decorate is-green"></hr>
                                                    <p className="c-gray-9 mb--6">Nhận phòng</p>
                                                    <p className="p--giant mb--6">05/10/2021</p>
                                                    <p className="p--small-2 c-gray-9 mb--0">Thứ ba</p>
                                                </div>
                                            </div>
                                            <div className="l-6 col bgr-white mlr--8 bd-radius-5">
                                                <div className="l-12 p--12">
                                                    <hr className="is-decorate is-orange"></hr>
                                                    <p className="c-gray-9 mb--0">Trả phòng</p>
                                                    <p className="p--giant mb--0">07/10/2021</p>
                                                    <p className="p--small-2 c-gray-9 mb--0">Thứ năm</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="check-out-left__input-group">
                                    <div className="check-out-left__sub-title">
                                        <b>Nội quy chỗ ở</b>
                                        <p className="input-group__text mt--8">Khách hàng chịu mọi trách nhiệm thiệt hại về tài sản đã gây ra tại chỗ ở trong thời gian lưu trú.</p>
                                    </div>
                                </div>    
                                <div className="check-out-left__input-group">
                                    <div className="check-out-left__sub-title">
                                        <b>Nội quy chỗ ở</b>
                                        <p className="input-group__text mt--8">Hạn chế làm ồn sau 10 giờ tối<br/>Không hút thuốc ở khu vực chung<br/></p>
                                    </div>
                                </div>                                 
                            </div>

                            <div className="check-out-left__title ">
                                <h3 className="mtb-48">Thông tin của bạn</h3>
                                <div className="check-out-left__input-group">
                                    <div className="check-out-left__sub-title mt--16">
                                        <span className="text-danger">* </span>
                                        <b>Tên Khách hàng</b>
                                        <p className="input-group__text mt--8">Họ tên trên CMND/ Thẻ căn cước</p>
                                        <input name="name" className="text-user-input" aria-required="true" aria-invalid="false"></input>
                                    </div>
                                    <div className="check-out-left__sub-title mt--16">
                                        <div className="is-flex mt--8">
                                            <div className="l-6 pr-8">
                                                <div className="l-12">                                                    
                                                    <span className="text-danger">*     </span>
                                                    <b>Số điện thoại</b>
                                                    <p className="input-group__text mt--8">Mã điện thoại quốc gia</p>
                                                    <input name="name" className="text-user-input" aria-required="true" aria-invalid="false"></input>
                                                </div>
                                            </div>
                                            <div className="l-6 pl-8">
                                                <div className="l-12">                                                    
                                                    <span className="text-danger">* </span>
                                                    <b>Email</b>
                                                    <p className="input-group__text mt--8">VD: email@example.com</p>
                                                    <input name="name" className="text-user-input" aria-required="true" aria-invalid="false"></input>
                                                </div>
                                            </div>                                            
                                        </div>
                                    </div>

                                    <div className="check-out-left__sub-title mt--16">
                                        <span className="text-danger">*</span>
                                        <b> Quốc gia cư trú</b>
                                        <p className="input-group__text mt--8">Nội dung này sẽ được sử dụng cho vấn đề pháp lý và thuế.</p>
                                        <div className="l-6 bgr-white mt--8 bd-radius-5">
                                            <div className="l-12 py--12 px--18 is-flex-btw">
                                                <span>Viet Nam</span>
                                                <i className="fas fa-chevron-down" style={{textAlign: 'center', fontSize:'10px', color: '#333', paddingTop:'8px'}}></i>
                                            </div>
                                        </div>
                                    </div>   
                                    <div className="check-out-left__sub-title mt--16">
                                        <div className="input-group input-group--checkbox checkbox-someone">
                                            <label className="input-group__label">
                                                <input type="checkbox"/> 
                                                <p className="d-inline">Tôi đặt phòng cho người khác</p>
                                            </label>
                                        </div>
                                    </div>                                  
                                </div>
                            </div>
                            <div className="check-out-left__title ">
                                <h3 className="mtb-48">Thông tin thêm</h3>
                                <div className="check-out-left__sub-title mt--16">
                                    <span className="text-danger">*</span>
                                    <b> Mục đích của chuyến đi này?</b>
                                    <div className="l-6 bgr-white mt--8 bd-radius-5">
                                        <div className="l-12 py--12 px--18 is-flex-btw">
                                            <span>Dành cho gia đình</span>
                                            <i className="fas fa-chevron-down" style={{textAlign: 'center', fontSize:'10px', color: '#333', paddingTop:'8px'}}></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="check-out-left__title ">
                                <h3 className="mtb-48">Mã khuyến mại</h3>
                                <div className="check-out-left__sub-title mt--16">
                                    <input name="name" className="text-user-input" aria-invalid="false" placeholder='Nhập mã khuyến mãi'></input>
                                </div>
                            </div>
                        </div>
                        <div style={{zIndex: '10'}}  onClick={ActiveAccept}>
                            <div id="checkout-btn" data-value="68886" disabled="disabled" className="btn btn--primary btn--sm is-flex-center">Thanh toán »</div>
                        </div>
                    </div>

                    <div className="check-out__info-center l-2"></div>
                    <div className="check-out__info-right l-4">
                        <div className="check-out-left__title ">
                            <h3 className="mb-48">Chi tiết đặt phòng</h3>
                        </div>

                        <div className="check-out-right__body">
                            <div className="check-up__header check-up__border-bottom">
                                <a href="# " className='check-up-header__link' style={{textDecoration: 'none', color: 'black'}}>
                                    <div className="check-up-header__left">
                                        <h4 className="check-out-left__sub-title">The Galaxy Home - 1 Phòng Ngủ, 60m2, View Thành Phố, Ban Công - Dịch Vọng</h4>
                                        <div>
                                            <i className="fas fa-map-marker-alt"></i>
                                            <span>Cầu giấy, Hà Nội, Vietnam</span>
                                        </div>                                        
                                    </div>
                                    <div className="check-up-header__right">
                                        <img src="../../img/location/location1.png" alt="" />
                                    </div>                                    
                                </a>
                            </div>
                            <div className="check-up__body">
                                <div className="check-up__details check-up__border-bottom">
                                    <div className="time-day">
                                        <svg version="1.1" viewBox="0 0 24 24" className="icon--24 svg-icon svg-fill">
                                            <g fill="none" fill-rule="evenodd">
                                                <path pid="0" fill="#F65E39" fill-rule="nonzero" d="M12 0c6.614 0 12 5.386 12 12s-5.386 12-12 12S0 18.614 0 12 5.386 0 12 0z"></path>
                                                <path pid="1" fill="#FFF" d="M10 7h4a1 1 0 112 0h.5A1.5 1.5 0 0118 8.5v8a1.5 1.5 0 01-1.5 1.5h-9A1.5 1.5 0 016 16.5v-8A1.5 1.5 0 017.5 7H8a1 1 0 112 0zm4 7v2h2v-2h-2zm-3 0v2h2v-2h-2zm-3 0v2h2v-2H8zm3-3v2h2v-2h-2zm3 0v2h2v-2h-2zm-6 0v2h2v-2H8z"></path>
                                            </g>
                                        </svg>
                                        <b>2 đêm . </b>
                                        <span> 05/10/2021 - 07/10/202</span>
                                    </div>
                                    <div className="time-day">
                                        <svg version="1.1" viewBox="0 0 24 24" className="icon--24 svg-icon svg-fill">
                                            <g fill="none"><circle pid="0" cx="12" cy="12" r="12" fill="#F65E39"></circle>
                                                <path pid="1" fill="#FFF" d="M12.007 12c.844 0 1.807-.083 2.414-.747.518-.539.681-1.396.518-2.585C14.702 6.995 13.607 6 12.007 6c-1.599 0-2.695.995-2.946 2.668-.163 1.19 0 2.046.518 2.585.607.664 1.57.747 2.428.747zm-3.67 6h7.335c.411 0 .779-.16 1.029-.442.264-.309.367-.724.264-1.126C16.436 14.408 14.408 13 12.012 13c-2.41 0-4.439 1.408-4.968 3.432-.103.402 0 .817.264 1.126.25.281.618.442 1.03.442z"></path>
                                            </g>
                                        </svg>
                                        <b>1 người lớn</b>
                                    </div>

                                </div>
                                <div className="check-up__price">
                                    <div className="check-up__border-bottom">
                                        <div className="is-flex-btw pt-20">
                                            <span>Giá thuê 2 đêm </span>     
                                            <span>1,600,000₫</span>                                   
                                        </div>
                                        <div className="is-flex-btw pt-20">
                                            <span>Phí dịch vụ </span>     
                                            <span>204,000₫</span>                                  
                                        </div>
                                    </div>
                                    <div className="check-up__border-bottom">
                                        <div className="is-flex-btw pt-20">
                                            <b>Tổng tiền </b>     
                                            <b>1,804,000₫</b>                                   
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="check-up__footer">
                                <p><b>Chính sách hủy phòng</b></p> 
                                <span>
                                    <b>Linh hoạt : </b>
                                    Miễn phí hủy phòng trong vòng 48h sau khi đặt phòng thành công và trước 1 ngày so với thời gian check-in. Sau đó, hủy phòng trước 1 ngày so với thời gian check-in, được hoàn lại 100% tổng số tiền đã trả (trừ phí dịch vụ).
                                    <span style={{color: 'var(--hover-color)'}}> Chi tiết</span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className={accept ? "page-checkout is-flex-btw" : " dis-none"} style={{marginTop: '79px', padding: '45px 90px', backgroundColor: '#f4f4f4'}}>
                <div className="check-out-left-right is-flex">
                    <div className="check-out__info-left">                                            
                        <div className="check-out-left__body">
                            <div className="check-out-left__title mt-0">
                                <h3 className="mb-14">Thông tin đặt chỗ</h3>                           
                                <p className="accept-sub-title">Vui lòng lựa chọn phương thức thanh toán</p>                                                               
                            </div>  
                            <div className="bgr-white is-shadow-box radius mt--18 w-80 ">
                                <label data-v-1bb36eb0="" role="radio" aria-checked tabindex="0" className="el-radio ml-0 is-payment">
                                    <div className="el-radio__input">
                                        <span className="el-radio__inner"></span>
                                        <input type="radio" name='banking' aria-hidden="true" tabindex="-1" className="el-radio__original" value="4"/>
                                    </div>
                                    <div className="el-radio__label">
                                        <div data-v-1bb36eb0="" className="gateway">
                                            <div data-v-1bb36eb0="" className="gateway__desc">
                                                <p data-v-1bb36eb0="" className="bold mb--0">Thẻ Visa, Thẻ Master, Thẻ JCB hoặc Thẻ American Express</p> 
                                                <span data-v-1bb36eb0="" className="c-gray-9 p--small-2 pt--6">Ghi nhận qua cổng thanh toán OnePay.</span>
                                            </div> 
                                            <div data-v-1bb36eb0="" className="gateway__icon">
                                                <img data-v-1bb36eb0="" src="https://cdn.luxstay.com/images/logos/payments/visa_master_jcb.svg" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </label>
                            </div>  
                            <div className="bgr-white is-shadow-box radius mt--18 w-80 ">
                                <label data-v-1bb36eb0="" role="radio" aria-checked tabindex="0" className="el-radio ml-0 is-payment">
                                    <div className="el-radio__input">
                                        <span className="el-radio__inner"></span>
                                        <input type="radio" name='banking' aria-hidden="true" tabindex="-1" className="el-radio__original" value="4"/>
                                    </div>
                                    <div className="el-radio__label">
                                        <div data-v-1bb36eb0="" className="gateway">
                                            <div data-v-1bb36eb0="" className="gateway__desc">
                                                <p data-v-1bb36eb0="" className="bold mb--0">Thẻ Visa, Thẻ Master, thẻ JCB</p> 
                                                <span data-v-1bb36eb0="" className="c-gray-9 p--small-2 pt--6">Nhập thông tin thẻ hoặc lựa chọn thẻ đã lưu để tiến hành thanh toán qua cổng Stripe.</span>
                                            </div> 
                                            <div data-v-1bb36eb0="" className="gateway__icon">
                                                <img data-v-1bb36eb0="" src="https://cdn.luxstay.com/images/logos/payments/visa_master_jcb.svg" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <div className="bgr-white is-shadow-box radius mt--18 w-80 ">
                                <label data-v-1bb36eb0="" role="radio" aria-checked tabindex="0" className="el-radio ml-0 is-payment">
                                    <div className="el-radio__input">
                                        <span className="el-radio__inner"></span>
                                        <input type="radio" name='banking' aria-hidden="true" tabindex="-1" className="el-radio__original" value="4"/>
                                    </div>
                                    <div className="el-radio__label">
                                        <div data-v-1bb36eb0="" className="gateway">
                                            <div data-v-1bb36eb0="" className="gateway__desc">
                                                <p data-v-1bb36eb0="" className="bold mb--0">ATM</p> 
                                                <span data-v-1bb36eb0="" className="c-gray-9 p--small-2 pt--6">Sử dụng thẻ ATM thanh toán qua cổng VNPT EPAY.</span>
                                            </div> 
                                            <div data-v-1bb36eb0="" className="gateway__icon">
                                                <img data-v-1bb36eb0="" src="https://cdn.luxstay.com/images/logos/payments/napas.svg" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <div className="bgr-white is-shadow-box radius mt--18 w-80 ">
                                <label data-v-1bb36eb0="" role="radio" aria-checked tabindex="0" className="el-radio ml-0 is-payment">
                                    <div className="el-radio__input">
                                        <span className="el-radio__inner"></span>
                                        <input type="radio" name='banking' aria-hidden="true" tabindex="-1" className="el-radio__original" value="4"/>
                                    </div>
                                    <div className="el-radio__label">
                                        <div data-v-1bb36eb0="" className="gateway">
                                            <div data-v-1bb36eb0="" className="gateway__desc">
                                                <p data-v-1bb36eb0="" className="bold mb--0">Chuyển khoản ngân hàng</p> 
                                                <span data-v-1bb36eb0="" className="c-gray-9 p--small-2 pt--6">Tiếp tục để nhận hướng dẫn thanh toán và thông tin ngân hàng từ Luxstay.</span>
                                            </div> 
                                            <div data-v-1bb36eb0="" className="gateway__icon">
                                                <img data-v-1bb36eb0="" src="https://cdn.luxstay.com/images/logos/payments/acb_vcb.svg" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <div className="bgr-white is-shadow-box radius mt--18 w-80 ">
                                <label data-v-1bb36eb0="" role="radio" aria-checked tabindex="0" className="el-radio ml-0 is-payment">
                                    <div className="el-radio__input">
                                        <span className="el-radio__inner"></span>
                                        <input type="radio" name='banking' aria-hidden="true" tabindex="-1" className="el-radio__original" value="4"/>
                                    </div>
                                    <div className="el-radio__label">
                                        <div data-v-1bb36eb0="" className="gateway">
                                            <div data-v-1bb36eb0="" className="gateway__desc">
                                                <p data-v-1bb36eb0="" className="bold mb--0">AlePay</p> 
                                                <span data-v-1bb36eb0="" className="c-gray-9 p--small-2 pt--6">Sử dụng thẻ thanh toán quốc tế để thanh toán qua cổng AlePay.</span>
                                            </div> 
                                            <div data-v-1bb36eb0="" className="gateway__icon">
                                                <img data-v-1bb36eb0="" src="https://cdn.luxstay.com/images/logos/payments/alepay.png" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <div className="bgr-white is-shadow-box radius mt--18 w-80 ">
                                <label data-v-1bb36eb0="" role="radio" aria-checked tabindex="0" className="el-radio ml-0 is-payment">
                                    <div className="el-radio__input">
                                        <span className="el-radio__inner"></span>
                                        <input type="radio" name='banking' aria-hidden="true" tabindex="-1" className="el-radio__original" value="4"/>
                                    </div>
                                    <div className="el-radio__label">
                                        <div data-v-1bb36eb0="" className="gateway">
                                            <div data-v-1bb36eb0="" className="gateway__desc">
                                                <p data-v-1bb36eb0="" className="bold mb--0">ZaloPay</p> 
                                                <span data-v-1bb36eb0="" className="c-gray-9 p--small-2 pt--6">Thanh toán bằng cổng thanh toán ZaloPay</span>
                                            </div> 
                                            <div data-v-1bb36eb0="" className="gateway__icon">
                                                <img data-v-1bb36eb0="" src="https://cdn.luxstay.com/images/logos/payments/zalopay.png" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <div className="bgr-white is-shadow-box radius mt--18 w-80 ">
                                <label data-v-1bb36eb0="" role="radio" aria-checked tabindex="0" className="el-radio ml-0 is-payment">
                                    <div className="el-radio__input">
                                        <span className="el-radio__inner"></span>
                                        <input type="radio" name='banking' aria-hidden="true" tabindex="-1" className="el-radio__original" value="4"/>
                                    </div>
                                    <div className="el-radio__label">
                                        <div data-v-1bb36eb0="" className="gateway">
                                            <div data-v-1bb36eb0="" className="gateway__desc">
                                                <p data-v-1bb36eb0="" className="bold mb--0">MoMo</p> 
                                                <span data-v-1bb36eb0="" className="c-gray-9 p--small-2 pt--6">Thanh toán bằng ví điện tử MoMo.</span>
                                            </div> 
                                            <div data-v-1bb36eb0="" className="gateway__icon">
                                                <img data-v-1bb36eb0="" src="https://cdn.luxstay.com/images/logos/payments/momo.png" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </label>
                            </div>                        
                            
                        </div>
                        <Link to="/locations/book-home/info-cast/QRcode" style= {{textDecoration: 'none'}}>
                            <div id="checkout-btn" data-value="68886" disabled="disabled" className="btn btn--primary btn--sm is-flex-center" title>Thanh toán ngay</div>
                        </Link>
                    </div>                    
                </div>
                <div className="check-out__info-right l-4 mt--27useHistory">
                    <div className="check-out-left__title">
                        <h3 className="mb-48">Chi tiết đặt phòng</h3>
                    </div>

                    <div className="check-out-right__body">
                        <div className="check-up__header check-up__border-bottom">
                            <a href="# " className='check-up-header__link' style={{textDecoration: 'none', color: 'black'}}>
                                <div className="check-up-header__left">
                                    <h4 className="check-out-left__sub-title">The Galaxy Home - 1 Phòng Ngủ, 60m2, View Thành Phố, Ban Công - Dịch Vọng</h4>
                                    <div>
                                        <i className="fas fa-map-marker-alt"></i>
                                        <span>Cầu giấy, Hà Nội, Vietnam</span>
                                    </div>                                        
                                </div>
                                <div className="check-up-header__right">
                                    <img src="../../img/location/location1.png" alt="" />
                                </div>                                    
                            </a>
                        </div>
                        <div className="check-up__body">
                            <div className="check-up__details check-up__border-bottom">
                                <div className="time-day">
                                    <svg version="1.1" viewBox="0 0 24 24" className="icon--24 svg-icon svg-fill">
                                        <g fill="none" fill-rule="evenodd">
                                            <path pid="0" fill="#F65E39" fill-rule="nonzero" d="M12 0c6.614 0 12 5.386 12 12s-5.386 12-12 12S0 18.614 0 12 5.386 0 12 0z"></path>
                                            <path pid="1" fill="#FFF" d="M10 7h4a1 1 0 112 0h.5A1.5 1.5 0 0118 8.5v8a1.5 1.5 0 01-1.5 1.5h-9A1.5 1.5 0 016 16.5v-8A1.5 1.5 0 017.5 7H8a1 1 0 112 0zm4 7v2h2v-2h-2zm-3 0v2h2v-2h-2zm-3 0v2h2v-2H8zm3-3v2h2v-2h-2zm3 0v2h2v-2h-2zm-6 0v2h2v-2H8z"></path>
                                        </g>
                                    </svg>
                                    <b>2 đêm . </b>
                                    <span> 05/10/2021 - 07/10/202</span>
                                </div>
                                <div className="time-day">
                                    <svg version="1.1" viewBox="0 0 24 24" className="icon--24 svg-icon svg-fill">
                                        <g fill="none"><circle pid="0" cx="12" cy="12" r="12" fill="#F65E39"></circle>
                                            <path pid="1" fill="#FFF" d="M12.007 12c.844 0 1.807-.083 2.414-.747.518-.539.681-1.396.518-2.585C14.702 6.995 13.607 6 12.007 6c-1.599 0-2.695.995-2.946 2.668-.163 1.19 0 2.046.518 2.585.607.664 1.57.747 2.428.747zm-3.67 6h7.335c.411 0 .779-.16 1.029-.442.264-.309.367-.724.264-1.126C16.436 14.408 14.408 13 12.012 13c-2.41 0-4.439 1.408-4.968 3.432-.103.402 0 .817.264 1.126.25.281.618.442 1.03.442z"></path>
                                        </g>
                                    </svg>
                                    <b>1 người lớn</b>
                                </div>

                            </div>
                            <div className="check-up__price">
                                <div className="check-up__border-bottom">
                                    <div className="is-flex-btw pt-20">
                                        <span>Giá thuê 2 đêm </span>     
                                        <span>1,600,000₫</span>                                   
                                    </div>
                                    <div className="is-flex-btw pt-20">
                                        <span>Phí dịch vụ </span>     
                                        <span>204,000₫</span>                                  
                                    </div>
                                </div>
                                <div className="check-up__border-bottom">
                                    <div className="is-flex-btw pt-20">
                                        <b>Tổng tiền </b>     
                                        <b>1,804,000₫</b>                                   
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="check-up__footer">
                            <p><b>Chính sách hủy phòng</b></p> 
                            <span>
                                <b>Linh hoạt : </b>
                                Miễn phí hủy phòng trong vòng 48h sau khi đặt phòng thành công và trước 1 ngày so với thời gian check-in. Sau đó, hủy phòng trước 1 ngày so với thời gian check-in, được hoàn lại 100% tổng số tiền đã trả (trừ phí dịch vụ).
                                <span style={{color: 'var(--hover-color)'}}> Chi tiết</span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <FooterBottom />
        </>
    )
}

export default InfoCast

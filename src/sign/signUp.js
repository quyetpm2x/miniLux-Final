import React from 'react'
import FooterBottom from '../home/footerBottom';
import HeaderSign from './headerSign';
import BodySignUp from './bodySignUp';

const SignUp = () => {
    
    return (
        <>
            <HeaderSign/>
            <BodySignUp />
            <FooterBottom />
        </>
    )
}

export default SignUp

import React from 'react'
import FooterBottom from '../home/footerBottom';
import HeaderSign from './headerSign';
import BodySignIn from './bodySignIn';

const SignIn = () => {
    return (
        <>
            <HeaderSign />
            <BodySignIn />
            <FooterBottom />
        </>
    )
}

export default SignIn

import React from 'react'
import '../base.css'
import '../grid.css'
import './bodySignUp.css'
import '../home/responsive.css'
import SignUpForm from './signUpForm';

const BodySignUp = () => {
    return (
        <> 
            <div className="bodySignUp">
                <div className="welcome">
                    <div className="welcome-content">
                        <h1 className="welcome-title">Đăng ký thành viên Luxstay - Tích điểm thưởng và nhận ưu đãi</h1>
                        <p className="welcome-desc">Nhanh chóng, tiện lợi và an toàn. Đăng ký liền tay, rinh ngay quyền lợi.</p>
                    </div>
                </div>
                <div className="section">
                    <div className="section-container">
                        <div className="row-0 is-flex">
                            <div className="c-8">
                                <div className="row-0 flex-wrap">
                                    <div className="c-6 pad-08">
                                        <div className="media">
                                            <img src = "img/sign/img-sign-1.png" alt="img-sign-1" style={{width: "65px", height: '70px'}}/>
                                            <h3 className="media__title">Tích điểm nhanh chóng</h3>
                                            <p className="media__content">Tích điểm đối với mỗi lượt đặt chỗ thành công. Quy đổi thành Lux Credit để du lịch nhiều hơn nữa.</p>
                                        </div>
                                    </div>
                                    <div className="c-6 pad-08">
                                        <div className="media">
                                            <img src = "img/sign/img-sign-2.png" alt="img-sign-2" style={{width: "65px", height: '70px'}}/>
                                            <h3 className="media__title">Tiện ích thông minh</h3>
                                            <p className="media__content">Check-in và kiểm tra hóa đơn thanh toán kể cả khi không có kết nối mạng. Hoàn tiền nhanh gọn. Đổi lịch dễ dàng.</p>
                                        </div>
                                    </div>
                                    <div className="c-6 pad-08">
                                        <div className="media">
                                            <img src = "img/sign/img-sign-3.png" alt="img-sign-3" style={{width: "65px", height: '70px'}}/>
                                            <h3 className="media__title">Thanh toán đơn giản</h3>
                                            <p className="media__content">Phương thức thanh toán tiện lợi, an toàn. Tích hợp chức năng lưu thẻ để đặt phòng lần sau.</p>
                                        </div>
                                    </div>
                                    <div className="c-6 pad-08">
                                        <div className="media">
                                            <img src = "img/sign/img-sign-4.png" alt="img-sign-4" style={{width: "65px", height: '70px'}}/>
                                            <h3 className="media__title">Ưu đãi mỗi ngày</h3>
                                            <p className="media__content">Nhận thông báo ưu đãi từ Luxstay khi có kế hoạch du lịch để lựa chọn và đặt ngay cho mình một chỗ ở phù hợp, tiện nghi với giá tốt nhất.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="c-4">
                                <SignUpForm />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </>
    )
}

export default BodySignUp

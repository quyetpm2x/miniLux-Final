import React from 'react'
import './QRcode.css'

const QRcode = () => {
    return (
        <div className="QRcode-wrapper">
            <div className="QRcode">
                <div className="QRcode-left">
                    <div className="QRcode-left__child mb--20">
                        <h3>Đơn hàng hết hạn sau</h3>
                        <div className="left__time">
                            <span>09:30</span>
                        </div>
                    </div>
                    <div className="QRcode-left__child mb--15 fz-20 border-bt-left">
                        <p className="lh-20">Nhà cung cấp</p>
                        <p className="pl-28 lh-20">Luxstay Việt Nam</p>
                    </div>
                    <div className="QRcode-left__child mtb-15 fz-16 border-bt-left">
                        <p className='left-money is-flex'>
                            <i class="fas fa-money-bill-alt"></i>
                            <p className='ml-10'>Số tiền</p>
                        </p>
                        <p style={{marginLeft:'27px'}}>1.904.000 vnđ</p>
                    </div>
                    <div className="QRcode-left__child mtb-15 fz-16 border-bt-left">
                        <p className='left-money is-flex'>
                            <i class="fas fa-money-check"></i>
                            <p className='ml-10'>Thông tin</p>
                        </p>
                        <p style={{marginLeft:'29px'}}>Luxstay</p>
                    </div>
                    <div className="QRcode-left__child mtb-15 fz-16">
                        <p className='left-money is-flex'>
                            <i class="fas fa-barcode"></i>
                            <p className='ml-10'>Đơn hàng</p>
                        </p>
                        <p style={{marginLeft:'23px'}}>1633321084_UUCHZM</p>
                    </div>
                    <div className="is-flex-center">
                        <div className="QRcode-left__child mtb-15 fz-16 is-flex is-abs">
                            <i class="fas fa-caret-left"></i>
                            <p style={{marginLeft:'15px'}}>quay lại</p>
                        </div>
                    </div>
                </div>
                <div className="QRcode-right">
                    <div className="right-header">
                        <div className="logo-lux">
                            <img src="https://cdn.mservice.io/static/gcs_partner_m4b/32277-2019-10-03/eAJI5YeCZqmBxWOIqSMs_1570089390292" alt="" />
                        </div>
                        <div className="logo-banking">
                            <img src="https://payment.momo.vn/gw_payment/faces/javax.faces.resource/material/img/logo-1.png" alt="" />
                        </div>
                    </div>
                    <div className="right-body">
                        <div className="right-sub-header">
                            <h2>Quét mã để thanh toán</h2>                            
                        </div>
                        <div className="right-qr-code">
                            <div className="qr-code-img">
                                <img src="https://payment.momo.vn/gw_payment/qrcode/image/receipt?key=c9c3888f3da99e6ff0c75bf5d2ab7c25ccac0773" alt="" />
                            </div>
                        </div>
                    </div>
                    <div className="right-footer">
                        <div className="footer-img-code">
                            <div className="is-flex-center lh-30">
                                <img src="https://payment.momo.vn/gw_payment/faces/javax.faces.resource/material/img/qr-code-1.png" alt="" />
                                <p className="ml-10">Sử dụng <b>Momo</b>  hoặc <br/> </p>
                            </div>
                             ứng dụng Camera hỗ trợ QR code để quét mã.
                            <div className="is-flex-center mt-16">                                
                                <i className="fas fa-spinner"></i>
                                <p className="ml-10">
                                    Đang chờ bạn quét mã
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default QRcode

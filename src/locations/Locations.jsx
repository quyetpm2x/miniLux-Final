import React, { useState } from 'react'
import Header from './../home/header';
import './location.css'
import { locationData } from './locationData.jsx';
import FooterBottom from './../home/footerBottom';
import { Link, useLocation } from 'react-router-dom';

const Locations = () => {
    const search = useLocation().search;
    const labelName = new URLSearchParams(search).get('label');

    const [stateSort, setStateSort] = useState({
        up: false,
        down: false,
        value: 'Lựa chọn'
    })

    const clickUp = () => {
        setStateSort(
            {
                up: !stateSort.up,
                down: false,
            }
        )
    }
    const clickDown = () => {
        setStateSort(
            {
                down: !stateSort.down,
                up: false,
            }
        )
    }

    return (
        <>
            <Header />
            <div className='header-after fixed'>
                <div className='filter c-12'>
                    <div className="filter-btn__wrapper">
                        <button className="filter-btn__click border-btn">
                            Hủy phòng linh hoạt
                        </button>
                    </div>
                    <div className="filter-btn__wrapper">
                        <button className="filter-btn__click border-btn">
                            Đặt phòng nhanh
                        </button>
                    </div>
                    <div className="filter-btn__wrapper">
                        <button className="filter-btn__click border-btn">
                            Giá ưu đãi
                        </button>
                    </div>
                    <div className="filter-btn__wrapper">
                        <button className="filter-btn__click border-btn">
                            Khu vực
                        </button>
                    </div>
                    <div className="filter-btn__wrapper">
                        <button className="filter-btn__click border-btn">
                            Loại phòng
                        </button>
                    </div>
                    <div className="filter-btn__wrapper">
                        <button className="filter-btn__click border-btn">
                            Giá chỗ ở
                        </button>
                    </div>
                    <div className="filter-btn__wrapper">
                        <button className="filter-btn__click border-btn">
                            Thêm bộ lọc
                        </button>
                    </div>
                </div>
            </div>
            <div className="body-locations__wrapper pad-0-45">
                <div className="text-covid">
                    <b>Trước khi đặt phòng, hãy kiểm tra những địa điểm bị hạn chế du lịch trong thời gian này. </b>
                    Sức khỏe và sự an toàn của cộng đồng luôn được đặt hàng đầu. Vì vậy, vui lòng làm theo chỉ thị của chính phủ bởi điều đó thực sự cần thiết.
                </div>

                <div className="location-wrapper">
                    <div className="location-title-sort">
                        <div className="location-title">
                            <h2>Homestay tại {labelName}</h2>
                        </div>

                        <div className="location-sort">
                            <div className="sort-text">
                                <p>Sắp xếp:</p>
                                <div className="location-options">Lựa chọn</div>
                                <i className="fas fa-chevron-down"></i>
                            </div>
                            <div className="sort-btn__wrapper">
                                <div className="sort-btn">
                                    <div className={stateSort.up ? "sort-up sort-active" : "sort-up"} onClick={clickUp}>Giá tăng dần</div>
                                    <div className={stateSort.down ? "sort-down sort-active" : "sort-down"} onClick={clickDown}>Giá giảm dần</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="body-locations-home mlr-8">
                    {locationData.map((homeInfo, index) => {
                        return (
                            <div className="l-2-4">
                                <div className="home-wrapprer col-pad-0-8">
                                    <Link to='/locations/book-home'>
                                        <img className="home-img" src={homeInfo.src} alt="" />
                                        <div className="home-decs">
                                            <p>{homeInfo.decs}</p>
                                            <div className="star">
                                                <i className="fas fa-star" style={{ color: '#ffb025' }}></i>
                                                <p style={{ color: 'black' }}>{homeInfo.numberStar}</p>
                                                <p>({homeInfo.personStar})</p>
                                            </div>
                                        </div>
                                        <div className="home-label">
                                            <svg data-v-ebd93bc0="" version="1.1" viewBox="0 0 20 20" className="is-instant-book mr--6 svg-icon svg-fill" style={{ width: '20px', height: '20px' }}><title>Đặt phòng ngay, không cần đợi chủ nhà phê duyệt</title><defs><linearGradient x1="100%" y1="50%" x2="0%" y2="50%" id="svgicon_instant_book_a"><stop stop-color="#F68A39" offset="0%"></stop><stop stop-color="#F65E39" offset="100%"></stop></linearGradient></defs><g fill="none" fill-rule="evenodd"><circle pid="0" fill="url(#svgicon_instant_book_a)" cx="10" cy="10" r="10"></circle><path pid="1" d="M10.42 15.158l3.037-6.073A.75.75 0 0012.787 8H10V5.177a.75.75 0 00-1.42-.335l-3.037 6.073A.75.75 0 006.213 12H9v2.823a.75.75 0 001.42.335z" fill="#FFF"></path></g></svg>
                                            <p>{homeInfo.label}</p>
                                        </div>
                                        <b className="home-price">{homeInfo.price}</b>
                                    </Link>
                                </div>
                            </div>

                        )
                    })
                    }
                </div>

                <div className="page-number">
                    <ul className="page-number__ul">
                        <li className="page-number__number page-prev">
                            <i className="fas fa-chevron-left"></i>
                        </li>
                        <li className="page-number__number page-active"><span>1</span></li>
                        <li className="page-number__number"><span>2</span></li>
                        <li className="page-number__number"><span>3</span></li>
                        <li className="page-number__number"><span>4</span></li>
                        <li className="page-number__number"><span>...</span></li>
                        <li className="page-number__number"><span>103</span></li>
                        <li className="page-number__number page-next">
                            <i className="fas fa-chevron-right"></i>
                        </li>
                    </ul>
                </div>

                <div className="after-page-number"><p>1 - 20 trong số 2077 Chỗ ở</p></div>
            </div>
            <div className="" style={{ borderBottom: '1px solid var(--border-color)', marginTop: '48px' }}></div>
            <FooterBottom />
        </>
    )
}

export default Locations

import React, { useState }  from 'react'
import styled from 'styled-components';

const SuggestionsLuxstaySlide = styled.div`
    
`;

const SuggestionsLuxstay = ({suggestionsLuxstayData}) => {
    const [current, setCurrent] = useState(0)
    const length = suggestionsLuxstayData.length    
 
    const nextSlide = () => {
        setCurrent(current === length - 4 ? 0 : current + 1)
    };

    const prevSlide = () => {
        setCurrent(current === 0 ? length - 4 : current - 1)
    };

    return (
        <>
        <div className="pro__place-btn-container">
            <button 
            className="slick-prev slick--disabled mb-70 hide-on-mobile"
            onClick={prevSlide}
            >
                <i className="far fa-chevron-left"></i>
            </button>
            <button 
            className="slick-next slick--enbled mb-70 hide-on-mobile"
            onClick={nextSlide}
            >
                <i className="far fa-chevron-right"></i>
            </button>
        </div>
        <div className="some__idea">
            <div className="grid wide">
                <div className="row sm-gutter">
                    <div className="col l-3 m-6 c-11">           
                        {suggestionsLuxstayData.map((suggestions, index) => {
                            return (                    
                                <SuggestionsLuxstaySlide key={index}>
                                    {index === current &&(   
                                        <div className="some__idea-container">
                                            <a href="# " className="some__idea-link">
                                                <img src={suggestions.src} alt="" className="some__idea-img" />
                                                <p className="some__idea-label">{suggestions.label}</p>
                                                <p className="some__idea-decs">{suggestions.decs}</p>
                                            </a>
                                        </div>                         
                                    )}                                            
                                </SuggestionsLuxstaySlide>    
                            )
                        })
                        }
                    </div>
                    <div className="col l-3 m-6 c-11">           
                        {suggestionsLuxstayData.map((suggestions, index) => {
                            return (                    
                                <SuggestionsLuxstaySlide key={index}>
                                    {index === current + 1 &&(   
                                        <div className="some__idea-container">
                                            <a href="# " className="some__idea-link">
                                                <img src={suggestions.src} alt="" className="some__idea-img" />
                                                <p className="some__idea-label">{suggestions.label}</p>
                                                <p className="some__idea-decs">{suggestions.decs}</p>
                                            </a>
                                        </div>                         
                                    )}                                            
                                </SuggestionsLuxstaySlide>    
                            )
                        })
                        }
                    </div>
                    <div className="col l-3 m-6 c-11">           
                        {suggestionsLuxstayData.map((suggestions, index) => {
                            return (                    
                                <SuggestionsLuxstaySlide key={index}>
                                    {index === current + 2 &&(   
                                        <div className="some__idea-container">
                                            <a href="# " className="some__idea-link">
                                                <img src={suggestions.src} alt="" className="some__idea-img" />
                                                <p className="some__idea-label">{suggestions.label}</p>
                                                <p className="some__idea-decs">{suggestions.decs}</p>
                                            </a>
                                        </div>                         
                                    )}                                            
                                </SuggestionsLuxstaySlide>    
                            )
                        })
                        }
                    </div>
                    <div className="col l-3 m-6 c-11">           
                        {suggestionsLuxstayData.map((suggestions, index) => {
                            return (                    
                                <SuggestionsLuxstaySlide key={index}>
                                    {index === current + 3 &&(   
                                        <div className="some__idea-container">
                                            <a href="# " className="some__idea-link">
                                                <img src={suggestions.src} alt="" className="some__idea-img" />
                                                <p className="some__idea-label">{suggestions.label}</p>
                                                <p className="some__idea-decs">{suggestions.decs}</p>
                                            </a>
                                        </div>                         
                                    )}                                            
                                </SuggestionsLuxstaySlide>    
                            )
                        })
                        }
                    </div>
                    
                </div>
            </div>
        </div>
        </>
    )
}

export default SuggestionsLuxstay

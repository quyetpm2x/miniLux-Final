import React, { useEffect, useState, useRef }  from 'react'
import styled from 'styled-components';

const SliderSection = styled.section`
    height: 100%;
    width: 100%;
    position: relative;
    overflow: hidden;
    max-height: 1296px;
    /* height:'291px' */

`;

const SliderWraper = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    overflow: hidden;
    position: relative;
    /* height:'291px' */

`;

const BodySliderSlide = styled.div`
    
`;


const BodySlider = ({slides}) => {
    const [current, setCurrent] = useState(0)
    const length = slides.length
    const timeout = useRef(null)

    
    useEffect(() =>{
        const nextSlide = () => {
            setCurrent(current === length -1 ? 0 : current + 1)
        };
        
        timeout.current = setTimeout(nextSlide, 3000);

        return function() {
            if(timeout.current) {
                clearTimeout(timeout.current);
            }
        };
    },[current, length])

    return (
        <SliderSection>
            <SliderWraper>
                {slides.map((slide, index) => {
                    return (
                        <BodySliderSlide key={index}>
                            {index === current &&(
                                <img src={slide.src} alt="" className="slider__img" />
                            )}
                        </BodySliderSlide>
                    )
                })
                }
            </SliderWraper>
        </SliderSection>
    )
}

export default BodySlider

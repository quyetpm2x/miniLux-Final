import React, {useState} from 'react'
import "../base.css"
import '../grid.css'
import "./responsive.css"
import "./header.css"
import {Link} from 'react-router-dom'


const Header = () => {
    const [blockState, setBlockState] = useState({
        date: false,
        cust: false,
        custNumber1: 0,
        custNumber2: 0,
        custNumber3: 0,
        lang: false,
    });

    const setBlockDate = () => {
        setBlockState({
            ...blockState,
            date: !blockState.date,
            cust: false,
        })
    }
    const setBlockCust = () => {
        setBlockState({
            ...blockState,
            date: false,
            cust: !blockState.cust,
        })
    }

    const plusCust1 = () => {
        setBlockState({
            ...blockState, 
            custNumber1: blockState.custNumber1 + 1})
    }

    const plusCust2 = () => {
        setBlockState({
            ...blockState, 
            custNumber2: blockState.custNumber2 + 1})
    }

    const plusCust3 = () => {
        setBlockState({
            ...blockState, 
            custNumber3: blockState.custNumber3 + 1})
    }

    const subCust1 = () => {
        if(blockState.custNumber1 > 0){
            setBlockState({
                ...blockState, 
                custNumber1: blockState.custNumber1 - 1
            })
        }
    }

    const subCust2 = () => {
        if(blockState.custNumber2 > 0){
            setBlockState({
                ...blockState, 
                custNumber2: blockState.custNumber2 - 1
            })
        }
    }

    const subCust3 = () => {
        if(blockState.custNumber3 > 0){
            setBlockState({
                ...blockState, 
                custNumber3: blockState.custNumber3 - 1
            })
        }
    }

    const setLang = () => {
        setBlockState({
            ...blockState, 
            lang: !blockState.lang,
        })
    }

    return (
        <div className="header">
                <div className="header__wrapper">
                    <nav className="header__navbar">
                        <div className="header__navbar-logo__box">
                            <div className="header__logo">
                                <Link to="/" className="header__logo-link">
                                    <svg version="1.1" viewBox="0 0 50 50" className="header__logo-img svg-icon svg-fill">
                                        <path
                                            pid="0"
                                            d="M25.093 0c13.781.06 24.94 11.317 24.882 25.106C49.917 38.894 38.663 50.058 24.88 50 11.1 49.942-.059 38.683.001 24.894.057 11.106 11.31-.058 25.092 0zm11.801 31.9L14.398 16.053c.241-.26.48-.518.74-.777 2.7-2.687 5.971-4.031 9.775-4.015 3.804.015 7.064 1.388 9.741 4.098.238.241.476.482.694.743l-5.951 4.133 2.381 1.688 5.153-3.576v.02L39.33 16.7c-.692-1.203-1.584-2.325-2.616-3.39-3.231-3.292-7.167-4.947-11.788-4.967-4.6-.019-8.53 1.603-11.809 4.867a17.806 17.806 0 00-2.682 3.408l1.429 1.004 23.429 16.51c-.24.26-.48.518-.74.777-2.7 2.687-5.971 4.011-9.775 3.996-3.803-.016-7.063-1.37-9.74-4.08-.258-.26-.496-.521-.734-.782l6.111-4.251-.139-.181-2.183-1.528-5.373 3.735v-.021l-2.377 1.65a17.824 17.824 0 002.654 3.43c3.253 3.291 7.169 4.946 11.769 4.965 4.62.021 8.549-1.602 11.83-4.866 1.06-1.075 1.96-2.21 2.682-3.406l-2.383-1.67z"
                                        ></path>
                                    </svg>                    
                                </Link>
                            </div>
                            <div className="header__search-box">                                
                                <div className="header__input">
                                    <div className="header__input-wrap">
                                        <i className="fad fa-search"></i>
                                        <input type="text" placeholder="Tìm kiếm" className="header__input-search"/>
                                        <div className="header__history">
                                            <h3 className="header__history-label">Tìm kiếm gần đây</h3>
                                            <ul className="header__history-list">
                                                <a href="# " className="header__history-link">
                                                    <li className="header__history-item">
                                                        <img src="img/location-hanoi.png" alt="" className="header__history-img"/>
                                                        <div className="header__history-content">
                                                            <h4 className="header__history-name">Phương Sài, Nha Trang, Khánh Hòa</h4>
                                                            <p className="header__history-desc">6 Chỗ ở</p>
                                                        </div>
                                                    </li>
                                                </a>
                                                <a href="# " className="header__history-link">
                                                    <li className="header__history-item">
                                                        <img src="img/location-hoian.png" alt="" className="header__history-img"/>
                                                        <div className="header__history-content">
                                                            <h4 className="header__history-name">Sài Đồng, Long Biên, Hà Nội</h4>
                                                            <p className="header__history-desc">3 Chỗ ở</p>
                                                        </div>
                                                    </li>
                                                </a>
                                                <a href="# " className="header__history-link">
                                                    <li className="header__history-item">
                                                        <img src="img/location-tphcm.png" alt="" className="header__history-img"/>
                                                        <div className="header__history-content">
                                                            <h4 className="header__history-name">Tổ hợp TSG Lotus Sài Đồng, Long Biên, Hà Nội</h4>
                                                            <p className="header__history-desc">1 Chỗ ở</p>
                                                        </div>
                                                    </li>
                                                </a>
                                                <a href="# " className="header__history-link">
                                                    <li className="header__history-item">
                                                        <img src="img/location-danang.png" alt="" className="header__history-img"/>
                                                        <div className="header__history-content">
                                                            <h4 className="header__history-name">Asahi Luxstay - TSG Lotus Sài Đồng Apartment</h4>
                                                            <p className="header__history-desc">3 Chỗ ở</p>
                                                        </div>
                                                    </li>
                                                </a>
                                                <a href="# " className="header__history-link">
                                                    <li className="header__history-item">
                                                        <img src="img/location-nhatrang.png" alt="" className="header__history-img"/>
                                                        <div className="header__history-content">
                                                            <h4 className="header__history-name">Moon Westlake Studio view vườn phố Trích Sài</h4>
                                                            <p className="header__history-desc">3 Chỗ ở</p>
                                                        </div>
                                                    </li>
                                                </a>
                                                <a href="# " className="header__history-link">
                                                    <li className="header__history-item">
                                                        <img src="img/location-vungtau.png" alt="" className="header__history-img"/>
                                                        <div className="header__history-content">
                                                            <h4 className="header__history-name">Đã nẵng</h4>
                                                            <p className="header__history-desc">1023 Chỗ ở</p>
                                                        </div>
                                                    </li>
                                                </a>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="header__field header__field-date hide-on-mobile">
                                    <div className="header__field-wrapper"  onClick={setBlockDate}>
                                        <i className="header__field-icon far fa-calendar"></i>
                                        <span className="header__field-label-date js-header__field-label-date">Ngày</span>
                                    </div>

                                    {/* MENU DATE */}

                                    <div className="close js-overlay"></div>
                                    <div className={blockState.date ? "header__field-month-calender block" : "header__field-month-calender"}>
                                        <div className="header__field-icon-box-left">
                                            <i className="header__field-month-icon <sm-gutter-left fas fa-chevron-left sm-gutter-left"></i>
                                        </div>
                                        <div className="header__field-icon-box-right">
                                            <i className="header__field-month-icon fas fa-chevron-right sm-gutter-right"></i>
                                        </div>
                                        <div className="header__field-month">

                                            <div className="header__field-left-month">
                                                <div className="header__field-label">
                                                    <h4 className="header__field-month-label">August 2021</h4>
                                                </div>
                                                <div className="header__field-weekend">
                                                    <div className="header__field-weekend-list">
                                                        <div className="header__field-weekend-item">Mon</div>
                                                        <div className="header__field-weekend-item">Tus</div>
                                                        <div className="header__field-weekend-item">Wed</div>
                                                        <div className="header__field-weekend-item">Thu</div>
                                                        <div className="header__field-weekend-item">Fri</div>
                                                        <div className="header__field-weekend-item">Sat</div>
                                                        <div className="header__field-weekend-item">Sun</div>
                                                    </div>
                                                </div>
                                                <div className="header__field-month-table">
                                                    <ul className="header__field-month-week">
                                                        <li className="header__field-month-day header__field-month-day--empty"></li>
                                                        <li className="header__field-month-day header__field-month-day--empty"></li>
                                                        <li className="header__field-month-day header__field-month-day--empty"></li>
                                                        <li className="header__field-month-day header__field-month-day--empty"></li>
                                                        <li className="header__field-month-day header__field-month-day--empty"></li>
                                                        <li className="header__field-month-day header__field-month-day--empty"></li>
                                                        <li className="header__field-month-day header__field-month-day--old">1</li>
                                                    </ul>
                                                    <ul className="header__field-month-week">
                                                        <li className="header__field-month-day header__field-month-day--old">2</li>
                                                        <li className="header__field-month-day header__field-month-day--old">3</li>
                                                        <li className="header__field-month-day header__field-month-day--old">4</li>
                                                        <li className="header__field-month-day header__field-month-day--old">5</li>
                                                        <li className="header__field-month-day header__field-month-day--old">6</li>
                                                        <li className="header__field-month-day header__field-month-day--old">7</li>
                                                        <li className="header__field-month-day header__field-month-day--old">8</li>
                                                    </ul>
                                                    <ul className="header__field-month-week">
                                                        <li className="header__field-month-day header__field-month-day--old">9</li>
                                                        <li className="header__field-month-day header__field-month-day--old">10</li>
                                                        <li className="header__field-month-day header__field-month-day--old">11</li>
                                                        <li className="header__field-month-day header__field-month-day--old">12</li>
                                                        <li className="header__field-month-day header__field-month-day--old">13</li>
                                                        <li className="header__field-month-day header__field-month-day--old">14</li>
                                                        <li className="header__field-month-day header__field-month-day--old">15</li>
                                                    </ul>
                                                    <ul className="header__field-month-week">
                                                        <li className="header__field-month-day header__field-month-day--old">16</li>
                                                        <li className="header__field-month-day header__field-month-day--old">17</li>
                                                        <li className="header__field-month-day header__field-month-day--old">18</li>
                                                        <li className="header__field-month-day header__field-month-day--old">19</li>
                                                        <li className="header__field-month-day header__field-month-day--old">20</li>
                                                        <li className="header__field-month-day header__field-month-day--old">21</li>
                                                        <li className="header__field-month-day header__field-month-day--old">22</li>
                                                    </ul>
                                                    <ul className="header__field-month-week">
                                                        <li className="header__field-month-day header__field-month-day--old">23</li>
                                                        <li className="header__field-month-day header__field-month-day--old">24</li>
                                                        <li className="header__field-month-day header__field-month-day--old">25</li>
                                                        <li className="header__field-month-day header__field-month-day--old">26</li>
                                                        <li className="header__field-month-day header__field-month-day--old">27</li>
                                                        <li className="header__field-month-day header__field-month-day--old">28</li>
                                                        <li className="header__field-month-day header__field-month-day--old">29</li>
                                                    </ul>
                                                    <ul className="header__field-month-week">
                                                        <li className="header__field-month-day header__field-month-day--old">30</li>
                                                        <li className="header__field-month-day header__field-month-day--old">31</li>
                                                        <li className="header__field-month-day header__field-month-day--empty"></li>
                                                        <li className="header__field-month-day header__field-month-day--empty"></li>
                                                        <li className="header__field-month-day header__field-month-day--empty"></li>
                                                        <li className="header__field-month-day header__field-month-day--empty"></li>
                                                        <li className="header__field-month-day header__field-month-day--empty"></li>
                                                    </ul>
                                                </div>
                                                <button className="btn__remove">Xóa</button>

                                            </div>
                                            <div className="header__field-right-month">
                                                <div className="header__field-label">
                                                    <h4 className="header__field-month-label">September 2021</h4>
                                                </div>
                                                <div className="header__field-weekend">
                                                    <div className="header__field-weekend-list">
                                                        <div className="header__field-weekend-item">Mon</div>
                                                        <div className="header__field-weekend-item">Tus</div>
                                                        <div className="header__field-weekend-item">Wed</div>
                                                        <div className="header__field-weekend-item">Thu</div>
                                                        <div className="header__field-weekend-item">Fri</div>
                                                        <div className="header__field-weekend-item">Sat</div>
                                                        <div className="header__field-weekend-item">Sun</div>
                                                    </div>
                                                </div>
                                                <div className="header__field-month-table">
                                                    <ul className="header__field-month-week">
                                                        <li className="header__field-month-day header__field-month-day--empty"></li>
                                                        <li className="header__field-month-day header__field-month-day--empty"></li>
                                                        <li className="header__field-month-day header__field-month-day--old">1</li>
                                                        <li className="header__field-month-day header__field-month-day--old">2</li>
                                                        <li className="header__field-month-day header__field-month-day--old">3</li>
                                                        <li className="header__field-month-day header__field-month-day--active">4</li>
                                                        <li className="header__field-month-day header__field-month-day--new">5</li>
                                                    </ul>
                                                    <ul className="header__field-month-week">
                                                        <li className="header__field-month-day header__field-month-day--new">6</li>
                                                        <li className="header__field-month-day header__field-month-day--new">7</li>
                                                        <li className="header__field-month-day header__field-month-day--new">8</li>
                                                        <li className="header__field-month-day header__field-month-day--new">9</li>
                                                        <li className="header__field-month-day header__field-month-day--new">10</li>
                                                        <li className="header__field-month-day header__field-month-day--new">11</li>
                                                        <li className="header__field-month-day header__field-month-day--new">12</li>
                                                    </ul>
                                                    <ul className="header__field-month-week">
                                                        <li className="header__field-month-day header__field-month-day--new">13</li>
                                                        <li className="header__field-month-day header__field-month-day--new">14</li>
                                                        <li className="header__field-month-day header__field-month-day--new">15</li>
                                                        <li className="header__field-month-day header__field-month-day--new">16</li>
                                                        <li className="header__field-month-day header__field-month-day--new">17</li>
                                                        <li className="header__field-month-day header__field-month-day--new">18</li>
                                                        <li className="header__field-month-day header__field-month-day--new">19</li>
                                                    </ul>
                                                    <ul className="header__field-month-week">
                                                        <li className="header__field-month-day header__field-month-day--new">20</li>
                                                        <li className="header__field-month-day header__field-month-day--new">21</li>
                                                        <li className="header__field-month-day header__field-month-day--new">22</li>
                                                        <li className="header__field-month-day header__field-month-day--new">23</li>
                                                        <li className="header__field-month-day header__field-month-day--new">24</li>
                                                        <li className="header__field-month-day header__field-month-day--new">25</li>
                                                        <li className="header__field-month-day header__field-month-day--new">26</li>
                                                    </ul>
                                                    <ul className="header__field-month-week">
                                                        <li className="header__field-month-day header__field-month-day--new">27</li>
                                                        <li className="header__field-month-day header__field-month-day--new">28</li>
                                                        <li className="header__field-month-day header__field-month-day--new">29</li>
                                                        <li className="header__field-month-day header__field-month-day--new">30</li>
                                                        <li className="header__field-month-day header__field-month-day--new">31</li>
                                                        <li className="header__field-month-day header__field-month-day--empty"></li>
                                                        <li className="header__field-month-day header__field-month-day--empty"></li>
                                                    </ul>
                                                    <ul className="header__field-month-week">
                                                        <li className="header__field-month-day header__field-month-day--empty"></li>
                                                        <li className="header__field-month-day header__field-month-day--empty"></li>
                                                        <li className="header__field-month-day header__field-month-day--empty"></li>
                                                        <li className="header__field-month-day header__field-month-day--empty"></li>
                                                        <li className="header__field-month-day header__field-month-day--empty"></li>
                                                        <li className="header__field-month-day header__field-month-day--empty"></li>
                                                        <li className="header__field-month-day header__field-month-day--empty"></li>
                                                    </ul>
                                                </div>
                                                <button className="btn__apply">Áp dụng</button>

                                            </div>
                                            
                                        </div>
                                        <div className="header__field-month-background"></div>
                                        
                                    {/* END: MENU DATE */}
                                        
                                    </div>
                                </div>
                                <div className="header__field header__field-member hide-on-mobile">
                                    <div className="header__field-wrapper" onClick={setBlockCust}>
                                        <i className="header__field-icon fas fa-user"></i>
                                        <span className="header__field-label js-header__field-label-member">Số khách</span>
                                    </div>

                                    {/* SET NUMBER */}
                                    <div className={blockState.cust ? "header__field-set-number block" : "header__field-set-number"}>
                                        <div className="header__field-label-container">
                                            <div className="">
                                                <div className="set__number-select">
                                                    <h4 className="set__number-select-label">Người lớn</h4>
                                                    <div className="set__number-select-option">
                                                    <div className={blockState.custNumber1 <= 0 ? "set__number-select-up-and-down set__number-select-up-and-down--disabled" : "set__number-select-up-and-down set__number-select-up-and-down--enabled"} onClick={subCust1}>
                                                        <i className="set__number-select-icon far fa-minus"></i>
                                                    </div>
                                                        <span className="set__number-select-number">{blockState.custNumber1}</span>
                                                    <div className="set__number-select-up-and-down set__number-select-up-and-down--enabled" onClick={plusCust1}>
                                                        <i className="set__number-select-icon far fa-plus"></i>
                                                    </div>
                                                    </div>
                                                </div>
                                                <div className="set__number-select mt-16">
                                                    <div className="set__number-title">
                                                        <h4 className="set__number-select-label">Trẻ em</h4>
                                                        <p className="set__number-select-desc">Tuổi từ 2-12 tuổi</p>
                                                    </div>
                                                    <div className="set__number-select-option">
                                                        <div className={blockState.custNumber2 <= 0 ? "set__number-select-up-and-down set__number-select-up-and-down--disabled" : "set__number-select-up-and-down set__number-select-up-and-down--enabled"} onClick={subCust2}>
                                                            <i className="set__number-select-icon far fa-minus"></i>
                                                        </div>
                                                        <span className="set__number-select-number">{blockState.custNumber2}</span>
                                                        <div className="set__number-select-up-and-down set__number-select-up-and-down--enabled" onClick={plusCust2}>
                                                            <i className="set__number-select-icon far fa-plus"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="set__number-select mt-16">
                                                    <div className="set__number-title">
                                                        <h4 className="set__number-select-label">Trẻ sơ sinh</h4>
                                                        <p className="set__number-select-desc">Dưới 2 tuổi</p>
                                                    </div>
                                                    <div className="set__number-select-option">
                                                        <div className={blockState.custNumber3 <= 0 ? "set__number-select-up-and-down set__number-select-up-and-down--disabled" : "set__number-select-up-and-down set__number-select-up-and-down--enabled"} onClick={subCust3}>
                                                            <i className="set__number-select-icon far fa-minus"></i>
                                                        </div>
                                                        <span className="set__number-select-number">{blockState.custNumber3}</span>
                                                        <div className="set__number-select-up-and-down set__number-select-up-and-down--enabled" onClick={plusCust3}>
                                                            <i className="set__number-select-icon far fa-plus"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div className="set__number-footer">
                                                    <button className="btn__remove none-margin">Xóa</button>
                                                    <button className="btn__apply none-margin">Áp dụng</button>
        
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {/* END: SET NUMBER */}
                                </div>
                                <button className="btn mtl-8 header__search-box-btn hide-on-mobile">
                                    <i className="far fa-search"></i>
                                </button>
                            </div>
                        </div>

                        <div className="header__mobile-bars">
                            <input type="checkbox" hidden className="header__mobile-bars-checkbox" id="header__mobile-bars-checkbox"/>
                            <label htmlFor="header__mobile-bars-checkbox" className="header__mobile-bars-icon">
                                <i className="fas fa-bars"></i>
                            </label>
                            <div className="header__mobile-bars-menu">
                                <label htmlFor="header__mobile-bars-checkbox" className="header__mobile-bars-close">
                                    <i className="fal fa-times"></i>
                                </label>
                                <div className="header__mobile-sort-bars">
                                    <div className="header__mobile-sort-bars-logo">
                                        <svg version="1.1" viewBox="0 0 244 50" className="svg-icon svg-fill" style={{width: '150px'}}>
                                            <path
                                                pid="0"
                                                d="M25.093 0c13.781.06 24.94 11.317 24.882 25.106C49.917 38.894 38.663 50.058 24.88 50 11.1 49.942-.059 38.683.001 24.894.057 11.106 11.31-.058 25.092 0zm11.801 31.9L14.398 16.053c.241-.26.48-.518.74-.777 2.7-2.687 5.971-4.031 9.775-4.015 3.804.015 7.064 1.388 9.741 4.098.238.241.476.482.694.743l-5.951 4.133 2.381 1.688 5.153-3.576v.02L39.33 16.7c-.692-1.203-1.584-2.325-2.616-3.39-3.231-3.292-7.167-4.947-11.788-4.967-4.6-.019-8.53 1.603-11.809 4.867a17.806 17.806 0 00-2.682 3.408l1.429 1.004 23.429 16.51c-.24.26-.48.518-.74.777-2.7 2.687-5.971 4.011-9.775 3.996-3.803-.016-7.063-1.37-9.74-4.08-.258-.26-.496-.521-.734-.782l6.111-4.251-.139-.181-2.183-1.528-5.373 3.735v-.021l-2.377 1.65a17.824 17.824 0 002.654 3.43c3.253 3.291 7.169 4.946 11.769 4.965 4.62.021 8.549-1.602 11.83-4.866 1.06-1.075 1.96-2.21 2.682-3.406l-2.383-1.67zm45.839 2.652l-12.45-.05.096-21.789a.957.957 0 00-.965-.945l-.678-.003a.96.96 0 00-.974.939v.047c-.048.095-.05.236-.05.33l-.098 22.636a1.337 1.337 0 001.351 1.326l13.805.056a.958.958 0 00.973-.938l.003-.662a1.037 1.037 0 00-1.013-.946v-.001zm27.64-22.67l-.871-.003c-.563-.003-1.027.42-1.027.937l-.075 15.878c-.014 3.157-3.252 5.735-7.197 5.72-3.943-.014-7.158-2.618-7.144-5.775l.123-15.5.052-.282a.957.957 0 00-.201-.756 1.128 1.128 0 00-.767-.332l-.87-.004c-.513-.002-.924.326-1.028.75a.857.857 0 00-.156.47l-.073 15.69c-.02 4.616 4.47 8.402 9.95 8.424 5.48.022 10.004-3.73 10.025-8.3l.122-15.5.054-.282a.952.952 0 00-.203-.755c-.152-.189-.458-.379-.714-.38zm48.638 11.226c-3.275-1.249-6.696-2.496-6.688-4.393.01-2.42 2.721-4.401 6.05-4.387 3.331.014 6.025 2.017 6.014 4.436l-.002.38a.96.96 0 00.961.953l.675.003a.96.96 0 00.971-.945v-.38c.009-1.66-.709-3.276-2.006-4.515-1.635-1.618-4.092-2.532-6.65-2.494-4.873-.02-8.698 3.047-8.715 6.938.03 3.748 4.416 5.38 8.656 6.962 3.324 1.247 6.795 2.495 6.785 4.487-.01 2.372-3.155 4.399-6.775 4.385-3.668-.016-6.747-2.07-6.737-4.44l.001-.38a.961.961 0 00-.96-.954l-.676-.002a.96.96 0 00-.97.945v.38c-.018 3.843 4.215 6.991 9.38 7.012 5.163.022 9.423-3.092 9.44-6.934-.033-3.796-4.464-5.427-8.754-7.057zM193.66 11.84l-18.844-.076c-.547-.003-.996.42-.999.94l-.002.66c-.002.52.443.946.99.949l8.103.032-.095 21.802c-.003.52.443.947.99.949l.696.002c.546.003.996-.42 1-.94l.095-21.802 8.055.031c.547.003.996-.42.999-.94l.003-.66c.003-.52-.443-.946-.99-.947zm49.264.515c-.144-.33-.481-.52-.867-.52l-.87-.005a.963.963 0 00-.677.28l-.145.14-.147.142-6.609 9.677-6.72-9.734c-.048-.046-.096-.094-.096-.14l-.143-.143a.97.97 0 00-.675-.285l-.774-.003a1.021 1.021 0 00-.872.515 1.03 1.03 0 00.045.988l7.966 11.576-.049 11.307c.006.527.437.95.964.946l.675.002a.957.957 0 00.97-.937l.049-11.308 7.923-11.509c.194-.329.243-.705.052-.989zm-37.898 12.047l3.75-7.994 3.728 8.023-7.478-.03zm15.615 10.954v-.047l-10.5-22.733c-.15-.425-.549-.71-1.102-.807-.603-.049-1.156.231-1.41.703v.046l-10.705 22.697v.047l-.204.471c-.095.283-.06.595.097.849.188.258.483.416.802.428l.854.004c.453.001.806-.282.958-.657l.05-.141 4.314-9.277 9.95.04 4.28 9.31.049.14c.15.38.5.664.952.667l.854.002c.352.002.654-.139.806-.422.152-.281.254-.563.104-.847l-.149-.473zm-86.008-15.15l.817.003a.963.963 0 00.729-.347l4.755-6.218.412-.478a.763.763 0 00.095-.915c-.135-.306-.453-.482-.816-.483l-.863-.004a.929.929 0 00-.868.608l-4.983 6.522a.94.94 0 00-.096.915c.135.22.453.394.816.397h.002zm-6.998 8.45l-.819-.002a.96.96 0 00-.728.347l-4.755 6.207-.413.48a.77.77 0 00-.094.92c.135.309.452.485.816.488l.864.002a.91.91 0 00.82-.478l5.03-6.601a.949.949 0 00.095-.92 1.027 1.027 0 00-.816-.443zm8.019-.453l.148.14 5.55 7.248c.248.284.246.662.095.99-.15.332-.5.519-.9.517l-.896-.004a1.013 1.013 0 01-.697-.286l-.149-.14c-.03-.002-.042-.02-.059-.044a.212.212 0 00-.04-.052l-5.501-7.151a1.005 1.005 0 01-.15-.237l-11.694-15.39-.446-.521c-.248-.285-.246-.662-.096-.993.15-.33.501-.518.9-.516l.897.004a.99.99 0 01.845.428l.15.19 11.992 15.721s.05.048.05.096z"
                                            ></path>
                                        </svg>                                    
                                    </div>
                                    <ul className="header__mobile-sort-bars-list">
                                        <li className="header__mobile-sort-bars-item">
                                            <a href="# " className="header__mobile-sort-bars-item-link">
                                                Guide
                                            </a>
                                        </li>
                                        <li className="header__mobile-sort-bars-item">
                                            <a href="# " className="header__mobile-sort-bars-item-link">
                                                Host
                                            </a>
                                        </li>
                                        <li className="header__mobile-sort-bars-item">
                                            <Link to="/signup" className="header__mobile-sort-bars-item-link">
                                                Đăng ký
                                            </Link>
                                        </li>
                                        <li className="header__mobile-sort-bars-item">
                                            <Link to="/signin" className="header__mobile-sort-bars-item-link">
                                                Đăng nhập
                                            </Link>
                                        </li>
                                        <li className="header__mobile-sort-bars-item">                                            
                                            <div className="header__mobile-sort-bars-translate__container">
                                                <label htmlFor="translate__box-checkbox" className="" style={{display: 'flex', width: '100%'}}>
                                                    <img src="img/vi.svg" alt="Việt Nam" className="header__mobile-sort-bars-item-img"/>
                                                    <span className="header__mobile-sort-bars-item-label">VND</span>
                                                    <i className="header__mobile-sort-bars-item-icon fas fa-caret-down"></i>
                                                </label>
                                                <input type="checkbox" hidden id="translate__box-checkbox" className="translate__box-checkbox"/>
                                                <div className="translate___box-container">
                                                    <ul className="translate__box-container-list">
                                                        <li className="translate__box-container-item">
                                                            <img src="img/vi.svg" alt="" className="translate__box-container-img"/>
                                                            <div className="translate__box-container-label">Tiếng Việt</div>
                                                            <i className="translate__box-container-icon far fa-check"></i>
                                                        </li>
                                                        <li className="translate__box-container-item">
                                                            <img src="img/en.svg" alt="" className="translate__box-container-img"/>
                                                            <div className="translate__box-container-label">English</div>
                                                        </li>
                                                        <li className="translate__box-container-item">
                                                            <img src="img/ko.svg" alt="" className="translate__box-container-img"/>
                                                            <div className="translate__box-container-label">Korean</div>
                                                        </li>
                                                        <li className="translate__box-container-item">
                                                            <div className="translate___box-container-price">VND</div>
                                                            <div className="translate__box-container-label">Việt Nam Đồng</div>
                                                            <i className="translate__box-container-icon far fa-check"></i>  
                                                        </li>
                                                        <li className="translate__box-container-item">
                                                            <div className="translate___box-container-price">USD</div>
                                                            <div className="translate__box-container-label">United States Dollar</div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                            <label htmlFor="header__mobile-bars-checkbox" className="header__mobile-overlay"></label>
                        </div>
                        <div className="header__select hide-on-mobile-tablet">
                            <ul className="header__list">
                                <li className="header__item">
                                    <a href="# " className="header__item-link">Guide</a>
                                </li>
                                <li className="header__item">
                                    <a href="# " className="header__item-link header__item-link--notify">Host</a>
                                </li>
                                <li className="header__item">
                                    <Link to="/signup" className="header__item-link">Đăng ký</Link>
                                </li>
                                <li className="header__item">
                                    <Link to="/signin" className="header__item-link">Đăng nhập</Link>
                                </li>
                                <li className="header__item">
                                    <div className="header__item-translate">
                                        <div className="header__item-translate-wrapper js-header__item-translate-wrapper">
                                            <img src="img/vi.svg" alt="Việt Nam" className="header__item-img"/>
                                            <span className="header__item-translate-label">VND</span>
                                            <i className="header__item-translate-icon fas fa-caret-down" onClick={setLang}></i>
                                            <div className={blockState.lang ? "header__translate js-header__translatex block" : "header__translate js-header__translatex"}>
                                                <div className="header__translate-container">
                                                    <div className="header__translate-lang">
                                                        <div className="header__translate-country">
                                                            <img src="img/vi.svg" alt="Việt Nam" className="header__translate-country-img"/>
                                                            <h4 className="header__translate-country-label">Tiếng Việt</h4>
                                                            <i className="header__translate-icon--active far fa-check"></i>
                                                        </div>
                                                        <div className="header__translate-country">
                                                            <img src="img/en.svg" alt="England" className="header__translate-country-img"/>
                                                            <h4 className="header__translate-country-label">English</h4>
                                                            
                                                        </div>
                                                        <div className="header__translate-country">
                                                            <img src="img/ko.svg" alt="Korea" className="header__translate-country-img"/>
                                                            <h4 className="header__translate-country-label">Korean</h4>
                                                            
                                                        </div>
                                                    </div>
                                                    <div className="header__translate-currency">
                                                        <div className="header__translate-unit">
                                                            <div className="header__translate-title">
                                                                <h4 className="header__translate-unit-label">VND</h4>
                                                                <span className="header__translate-unit-decs">Việt Nam Đồng</span>
                                                            </div>
                                                            <i className="header__translate-icon--active far fa-check"></i>
                                                        </div>
                                                        <div className="header__translate-unit">
                                                            <div className="header__translate-title">
                                                                <h4 className="header__translate-unit-label">USD</h4>
                                                                <span className="header__translate-unit-decs">United States Dollar</span>
                                                            
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
    )
}

export default Header

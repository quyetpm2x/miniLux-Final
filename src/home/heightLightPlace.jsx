import React, { useState }  from 'react'
import styled from 'styled-components';
import { LabelContext } from '../context';
import { useContext } from 'react';
import { Link } from 'react-router-dom';

const HeightLightPlaceSlide = styled.div`
    
`;

const HeightLightPlace = ({heightLightPlaceData}) => {
    const label = useContext(LabelContext)

    const [current, setCurrent] = useState(0)
    const length = heightLightPlaceData.length  
 
    const nextSlide = () => {
        setCurrent(current === length - 5 ? 0 : current + 1)
    };

    const prevSlide = () => {
        setCurrent(current === 0 ? length - 5 : current - 1)
    };

    // const heighLightPlaceConText = useContext(PlaceContext);

    return (
        <>
        <div className="pro__place-btn-container">
            <button 
            className="slick-prev slick--disabled hide-on-mobile"
            onClick={prevSlide}
            >
                <i className="far fa-chevron-left"></i>
            </button>
            <button 
            className="slick-next slick--enbled hide-on-mobile"
            onClick={nextSlide}
            >
                <i className="far fa-chevron-right"></i>
            </button>
        </div>
        <div className="pro__place">
            <div className="grid wide">
                <div className="row sm-gutter">
                    <div className="col l-2-4 m-4 c-5">            
                        {heightLightPlaceData.map((placePro, index) => {
                            return (       
                                <HeightLightPlaceSlide key={index}>
                                    {index === current &&(   
                                        <div>
                                            <Link to={`/locations?label=${placePro?.label}`} className="pro__place-link">
                                                <img src={placePro.src} alt="Hà Nội" className="pro__place-img" />
                                                <div className="pro__place-title">
                                                    <h3 className="pro__place-label">{label[index].label}</h3>
                                                    <span className="pro__place-decs">{placePro.decs} </span>
                                                    <span className="pro__place-decs-place">{placePro.decsPlace}</span>
                                                </div>
                                            </Link>
                                        </div>                            
                                    )}                                            
                                </HeightLightPlaceSlide> 
                            )
                        })
                        }
                    </div>
                    <div className="col l-2-4 m-4 c-5">            
                        {heightLightPlaceData.map((placePro, index) => {
                            return (                    
                                <HeightLightPlaceSlide key={index}>
                                    {index === current + 1 &&(   
                                        <div>
                                            <Link to={`/locations?label=${placePro?.label}`} className="pro__place-link">
                                                <img src={placePro.src} alt="Hà Nội" className="pro__place-img" />
                                                <div className="pro__place-title">
                                                    <h3 className="pro__place-label">{label[index].label}</h3>
                                                    <span className="pro__place-decs">{placePro.decs} </span>
                                                    <span className="pro__place-decs-place">{placePro.decsPlace}</span>
                                                </div>
                                            </Link>
                                        </div>                            
                                    )}                                            
                                </HeightLightPlaceSlide>    
                            )
                        })
                        }
                    </div>
                    <div className="col l-2-4 m-4 c-5">            
                        {heightLightPlaceData.map((placePro, index) => {
                            return (                    
                                <HeightLightPlaceSlide key={index}>
                                    {index === current + 2 && (   
                                        <div>
                                            <Link to={`/locations?label=${placePro?.label}`} className="pro__place-link">
                                                <img src={placePro.src} alt="Hà Nội" className="pro__place-img" />
                                                <div className="pro__place-title">
                                                    <h3 className="pro__place-label">{label[index].label}</h3>
                                                    <span className="pro__place-decs">{placePro.decs} </span>
                                                    <span className="pro__place-decs-place">{placePro.decsPlace}</span>
                                                </div>
                                            </Link>
                                        </div>                            
                                    )}                                            
                                </HeightLightPlaceSlide>    
                            )
                        })
                        }
                    </div>
                    <div className="col l-2-4 m-4 c-5">            
                        {heightLightPlaceData.map((placePro, index) => {
                            return (                    
                                <HeightLightPlaceSlide key={index}>
                                    {index === current + 3 && (   
                                        <div>
                                            <Link to={`/locations?label=${placePro?.label}`} className="pro__place-link">
                                                <img src={placePro.src} alt="Hà Nội" className="pro__place-img" />
                                                <div className="pro__place-title">
                                                    <h3 className="pro__place-label">{label[index].label}</h3>
                                                    <span className="pro__place-decs">{placePro.decs} </span>
                                                    <span className="pro__place-decs-place">{placePro.decsPlace}</span>
                                                </div>
                                            </Link>
                                        </div>                            
                                    )}                                            
                                </HeightLightPlaceSlide>    
                            )
                        })
                        }
                    </div>
                    <div className="col l-2-4 m-4 c-5">            
                        {heightLightPlaceData.map((placePro, index) => {
                            return (                    
                                <HeightLightPlaceSlide key={index}>
                                    {index === current + 4 && (     
                                        <div>
                                            <Link to={`/locations?label=${placePro?.label}`} className="pro__place-link">
                                                <img src={placePro.src} alt="Hà Nội" className="pro__place-img" />
                                                <div className="pro__place-title">
                                                    <h3 className="pro__place-label">{label[index].label}</h3>
                                                    <span className="pro__place-decs">{placePro.decs} </span>
                                                    <span className="pro__place-decs-place">{placePro.decsPlace}</span>
                                                </div>
                                            </Link>
                                        </div>                            
                                    )}                                            
                                </HeightLightPlaceSlide>    
                            )
                        })
                        }
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}

// const HeightLightPlaceContext = (props)  => {
//     return (
//     <Provider>
//         <HeightLightPlace heightLightPlaceData={props.heightLightPlaceData}/>
//     </Provider>)
// }

// export default HeightLightPlaceContext
export default HeightLightPlace

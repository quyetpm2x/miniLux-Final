import React, { useState }  from 'react'
import styled from 'styled-components';

const GoodWillSlide = styled.div`
    
`;

const GoodWill = ({goodWillData}) => {
    const [current, setCurrent] = useState(0)
    const length = goodWillData.length    
 
    const nextSlide = () => {
        setCurrent(current === length - 3 ? 0 : current + 1)
    };

    const prevSlide = () => {
        setCurrent(current === 0 ? length - 3 : current - 1)
    };

    return (
        <>
        <div className="safe__off-btn-container">
            <button 
            className="slick-prev slick--disabled hide-on-mobile mb-10"
            onClick={prevSlide}
            >
                <i className="far fa-chevron-left"></i>
            </button>
            <button 
            className="slick-next slick--enbled hide-on-mobile mb-10"
            onClick={nextSlide}            
            >
                <i className="far fa-chevron-right"></i>
            </button>
        </div>

        <div className="sale__off">                               
            <div className="grid wide">
                <div className="row sm-gutter">
                    <div className="col l-4 m-6 c-11">            
                        {goodWillData.map((goodwill, index) => {
                            return (                    
                                <GoodWillSlide key={index}>
                                    {index === current &&(   
                                        <div className="sale__off-container">
                                            <a href="# " className="sale__off-link">
                                                <img   src={goodwill.src} alt="" className="sale__off-img" />
                                            </a>
                                        </div>                         
                                    )}                                            
                                </GoodWillSlide>    
                            )
                        })
                        }
                    </div>
                    <div className="col l-4 m-6 c-11">            
                        {goodWillData.map((goodwill, index) => {
                            return (                    
                                <GoodWillSlide key={index}>
                                    {index === current + 1 &&(   
                                        <div className="sale__off-container">
                                            <a href="# " className="sale__off-link">
                                                <img   src={goodwill.src} alt="" className="sale__off-img" />
                                            </a>
                                        </div>                         
                                    )}                                            
                                </GoodWillSlide>    
                            )
                        })
                        }
                    </div>
                    <div className="col l-4 m-6 c-11">            
                        {goodWillData.map((goodwill, index) => {
                            return (                    
                                <GoodWillSlide key={index}>
                                    {index === current + 2 &&(   
                                        <div className="sale__off-container">
                                            <a href="# " className="sale__off-link">
                                                <img   src={goodwill.src} alt="" className="sale__off-img" />
                                            </a>
                                        </div>                         
                                    )}                                            
                                </GoodWillSlide>    
                            )
                        })
                        }
                    </div>       
                </div>
            </div>
        </div>
        </>
    )
}

export default GoodWill

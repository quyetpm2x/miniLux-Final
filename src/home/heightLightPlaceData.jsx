export const HeightLightPlaceData = [
    {
        src:'img/location-hanoi.png',
        label:'Hà Nội',
        decs: '2822',
        decsPlace:'Chỗ ở'
    },
    {
        src:'img/location-tphcm.png',
        label:'TP. Hồ Chí Minh',
        decs: '2893',
        decsPlace:'Chỗ ở'
    },
    {
        src:'img/location-hoian.png',
        label:'Hội An',
        decs: '534',
        decsPlace:'Chỗ ở'
    },
    {
        src:'img/location-dalat.png',
        label:'Đà Lạt',
        decs: '222',
        decsPlace:'Chỗ ở'
    },
    {
        src:'img/location-danang.png',
        label:'Đà Nẵng',
        decs: '211',
        decsPlace:'Chỗ ở'
    },
    {
        src:'img/location-nhatrang.png',
        label:'Nha Trang',
        decs: '200',
        decsPlace:'Chỗ ở'
    },
    {
        src:'img/location-vungtau.png',
        label:'Vũng tàu',
        decs: '255',
        decsPlace:'Chỗ ở'
    },
]

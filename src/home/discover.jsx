import React, { useState }  from 'react'
import styled from 'styled-components';

const DiscoverSlide = styled.div`
    
`;

const Discover = ({discover}) => {
    const [current, setCurrent] = useState(0)
    const length = discover.length    
 
    const nextSlide = () => {
        setCurrent(current === length - 3 ? 0 : current + 1)
    };

    const prevSlide = () => {
        setCurrent(current === 0 ? length - 3 : current - 1)
    };

    return (
        <>
        <div className="safe__off-btn-container">
            <button 
            className="slick-prev slick--disabled hide-on-mobile"
            onClick={prevSlide}
            >
                <i className="far fa-chevron-left"></i>
            </button>
            <button 
            className="slick-next slick--enbled hide-on-mobile"
            onClick={nextSlide}       
            >
                <i className="far fa-chevron-right"></i>
            </button>
        </div>

        <div className="some__discover">
            <div className="grid wide">
                <div className="row sm-gutter">
                    <div className="col l-4 m-12 c-11">            
                        {discover.map((disco, index) => {
                            return (                    
                                <DiscoverSlide key={index}>
                                    {index === current &&(   
                                        <div className="some__discover-item">
                                            <a href="# " className="some__discover-item-link">
                                                <img src={disco.src} alt="" className="some__discover-item-img" />
                                            </a>
                                            <div className="some__discover-item-label">{disco.label}</div>
                                            <div className="some__discover-item-decs">{disco.dics}</div>
                                        </div>                        
                                    )}                                            
                                </DiscoverSlide>    
                            )
                        })
                        }
                    </div>
                    <div className="col l-4 m-12 c-11">            
                        {discover.map((disco, index) => {
                            return (                    
                                <DiscoverSlide key={index}>
                                    {index === current + 1 &&(   
                                        <div className="some__discover-item">
                                            <a href="# " className="some__discover-item-link">
                                                <img src={disco.src} alt="" className="some__discover-item-img" />
                                            </a>
                                            <div className="some__discover-item-label">{disco.label}</div>
                                            <div className="some__discover-item-decs">{disco.dics}</div>
                                        </div>                        
                                    )}                                            
                                </DiscoverSlide>    
                            )
                        })
                        }
                    </div>
                    <div className="col l-4 m-12 c-11">            
                        {discover.map((disco, index) => {
                            return (                    
                                <DiscoverSlide key={index}>
                                    {index === current + 2 &&(   
                                        <div className="some__discover-item">
                                            <a href="# " className="some__discover-item-link">
                                                <img src={disco.src} alt="" className="some__discover-item-img" />
                                            </a>
                                            <div className="some__discover-item-label">{disco.label}</div>
                                            <div className="some__discover-item-decs">{disco.dics}</div>
                                        </div>                        
                                    )}                                            
                                </DiscoverSlide>    
                            )
                        })
                        }
                    </div>
                        
                </div>
            </div>
        </div>
        </>
    )
}

export default Discover

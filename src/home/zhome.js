import Body from './body.jsx';
import Header from './header.jsx';
import Footer from './footer.jsx';

function Home() {
  return (
    <>
      <Header />
      <Body />  
      <Footer />
    </>
  );
}

export default Home;
import React, { useState }  from 'react'
import styled from 'styled-components';

const GuideSlide = styled.div`
    
`;

const Guide = ({guides}) => {
    const [current, setCurrent] = useState(0)
    const length = guides.length    
 
    const nextSlide = () => {
        setCurrent(current === length - 5 ? 0 : current + 1)
    };

    const prevSlide = () => {
        setCurrent(current === 0 ? length - 5 : current - 1)
    };

    return (
        <>
        <div className="guide-btn-container">
            <button 
            className="slick-prev slick--disabled mb-10"
            onClick={prevSlide}            
            >
                <i className="far fa-chevron-left"></i>
            </button>
            <button 
            className="slick-next slick--enbled mb-10"
            onClick={nextSlide}            
            >
                <i className="far fa-chevron-right"></i>
            </button>
        </div>
        <div className="guide">
            <div className="grid wide">
                <div className="row sm-gutter" style={{overflow: 'hidden'}}>
                    <div className="col l-2-4 m-6 c-11">           
                        {guides.map((guideImages, index) => {
                            return (                    
                                <GuideSlide key={index}>
                                    {index === current &&(   
                                        <div className="guide-item">
                                            <a href="# " className="guide-item-link">
                                                <img src={guideImages.src} alt="" className="guide-item-img" />
                                            </a>
                                        </div>                        
                                    )}                                            
                                </GuideSlide>    
                            )
                        })
                        }
                    </div>
                    <div className="col l-2-4 m-6 c-11">           
                        {guides.map((guideImages, index) => {
                            return (                    
                                <GuideSlide key={index}>
                                    {index === current + 1 &&(   
                                        <div className="guide-item">
                                            <a href="# " className="guide-item-link">
                                                <img src={guideImages.src} alt="" className="guide-item-img" />
                                            </a>
                                        </div>                        
                                    )}                                            
                                </GuideSlide>    
                            )
                        })
                        }
                    </div>
                    <div className="col l-2-4 m-6 c-11">           
                        {guides.map((guideImages, index) => {
                            return (                    
                                <GuideSlide key={index}>
                                    {index === current + 2 &&(   
                                        <div className="guide-item">
                                            <a href="# " className="guide-item-link">
                                                <img src={guideImages.src} alt="" className="guide-item-img" />
                                            </a>
                                        </div>                        
                                    )}                                            
                                </GuideSlide>    
                            )
                        })
                        }
                    </div>
                    <div className="col l-2-4 m-6 c-11">           
                        {guides.map((guideImages, index) => {
                            return (                    
                                <GuideSlide key={index}>
                                    {index === current + 3 &&(   
                                        <div className="guide-item">
                                            <a href="# " className="guide-item-link">
                                                <img src={guideImages.src} alt="" className="guide-item-img" />
                                            </a>
                                        </div>                        
                                    )}                                            
                                </GuideSlide>    
                            )
                        })
                        }
                    </div>
                    <div className="col l-2-4 m-6 c-11">           
                        {guides.map((guideImages, index) => {
                            return (                    
                                <GuideSlide key={index}>
                                    {index === current + 4 &&(   
                                        <div className="guide-item">
                                            <a href="# " className="guide-item-link">
                                                <img src={guideImages.src} alt="" className="guide-item-img" />
                                            </a>
                                        </div>                        
                                    )}                                            
                                </GuideSlide>    
                            )
                        })
                        }
                    </div>
                    
                </div>
            </div>
        </div>
        </>
    )
}

export default Guide

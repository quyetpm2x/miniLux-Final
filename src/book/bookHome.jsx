import React, {useState} from 'react'
import HeaderSign from './../sign/headerSign';
import { BookHomeData } from './bookHomeData';
import './bookHome.css';
import '../grid.css';
import '../base.css';
import FooterBottom from './../home/footerBottom';
import { locationData } from './../locations/locationData';
import { Link } from 'react-router-dom';
import { LabelContext } from '../context';
import { useContext } from 'react';

const BookHome = ({indexLocation}) => {

    const label = useContext(LabelContext)

    const [blockState, setBlockState] = useState({
        cust: false,
        custNumber1: 1,
        custNumber2: 0,
        custNumber3: 0,
    });

    const setBlockCust = () => {
        setBlockState({
            ...blockState,
            cust: !blockState.cust,
        })
    }

    const setBlockCustDelete = () => {
        setBlockState({
            ...blockState,
            cust: !blockState.cust,
            custNumber1: 1,
            custNumber2: 0,
            custNumber3: 0,
        })
    }

    const plusCust1 = () => {
        setBlockState({
            ...blockState, 
            custNumber1: blockState.custNumber1 + 1})
    }

    const plusCust2 = () => {
        setBlockState({
            ...blockState, 
            custNumber2: blockState.custNumber2 + 1})
    }

    const plusCust3 = () => {
        setBlockState({
            ...blockState, 
            custNumber3: blockState.custNumber3 + 1})
    }

    const subCust1 = () => {
        if(blockState.custNumber1 > 1){
            setBlockState({
                ...blockState, 
                custNumber1: blockState.custNumber1 - 1
            })
        }
    }

    const subCust2 = () => {
        if(blockState.custNumber2 > 0){
            setBlockState({
                ...blockState, 
                custNumber2: blockState.custNumber2 - 1
            })
        }
    }

    const subCust3 = () => {
        if(blockState.custNumber3 > 0){
            setBlockState({
                ...blockState, 
                custNumber3: blockState.custNumber3 - 1
            })
        }
    }

    const numberCusts = blockState.custNumber1 + blockState.custNumber2 + blockState.custNumber3;
    
    return (
        <>
            <HeaderSign />
            <div className="book-home-body" style={{marginTop: '79px'}}>
                <div className="book-home__slide-wrapper">
                    <div className="book-home__slide">
                        <div className="book-home__slide-prev">

                        </div>
                        {
                            BookHomeData.map((home, index) => {
                                return (
                                    <div className="book-home__wrapper-img">
                                        <div className="book-home__backgroung">
                                            <img   src={home.src} alt="" className='book-home__img'/>       
                                        </div>
                                    </div>
                                )
                            })
                        }

                        <div className="book-home__slide-prev">

                        </div>
                    </div>

                    <div className="dis-home">
                        <div className="dis-home-dots">
                            <i className="fas fa-ellipsis-v"></i>
                            <i className="fas fa-ellipsis-v"></i>
                            <i className="fas fa-ellipsis-v"></i>
                        </div>
                        <span>Khám phá chỗ ở</span>
                    </div>

                    <div className="code-home">
                        <span className="code-home__text">Mã chỗ ở</span>
                        <span className="code-home__number">68886</span>
                    </div>
                </div>
                <div className="book-home-left-right row-143" style={{display: 'flex'}}>
                    <div className="book-home-left col l-8 pad-right-50">
                        <div className="left-address">
                            <a href="# ">Luxstay</a>
                            <a href="# ">Vietnam</a>
                            <a href="# ">Ha Noi</a>
                            <a href="# ">Cau Giay</a>
                            <a href="# ">Dich Vong</a>
                        </div>
                        <div className="left-title">
                            <h1 >The Galaxy Home - 1 Phòng Ngủ, 60m2, View Thành Phố, Ban Công - Dịch Vọng </h1>
                            <div className="left-home__address">
                                <i className="fas fa-map-marker-alt"></i>
                                <b>Cầu Giấy, Hà Nội, Vietnam </b>
                                <a href="# " className="left-home__address-map">Xem bản đồ</a>                                
                            </div>
                            <div className="left-home__address">
                                <i className="fas fa-bell"></i>
                                <b>Căn hộ dịch vụ ·</b>
                                <span>55m2</span>
                            </div>
                            <div className="left-home-desc">
                                <span>Phòng riêng · </span>
                                <span>1 Phòng tắm · </span>
                                <span>1 giường · </span>
                                <span> 1 phòng ngủ ·</span>
                                <span>2 khách (tối đa 3 khách)</span>
                            </div>

                            <div className="left-home-about">
                                <ul className="left-home-about-ul">Tóm tắt về The Galaxy Home Apartment</ul>
                                    <li>Vị trí rất đẹp và thuận tiện ở quận Cầu Giấy</li>
                                    <li>Gần công viên Cầu Giấy, Lotteria, trung tâm mua sắm với môi trường ngoài trời yên tĩnh</li>
                                    <li>Bạn hoàn toàn có thể trải nghiệm những dịch vụ cao cấp tại đây</li>
                                <ul className="left-home-about-ul">Về không gian</ul>
                                    <li>Căn hộ được thiết kế với nhiều lựa chọn bố trí hợp lý và được trang bị theo tiêu chuẩn cao cấp 4 sao với ban công riêng và cảnh quan đẹp</li>
                                    <li>Có nhiều dịch vụ tại chỗ khác nhau như giặt ủi, dịch vụ vệ sinh, Wi-Fi miễn phí tốc độ cao, an ninh 24/7</li>
                                    <li>Dịch vụ khách hàng đặc biệt được cung cấp</li>
                                <ul className="left-home-about-ul">Căn hộ Deluxe 60m2 - Có ban công - Hướng nhìn thành phố</ul>
                                    <li>Thang máy ra vào căn hộ với hệ thống thẻ khóa an ninh</li>
                                    <li>Phòng khách được thiết kế theo phong cách hiện đại với ghế sofa và khu vực ăn uống riêng</li>
                                    <li>Nhà bếp được trang bị đầy đủ với bếp điện và máy hút mùi điện, lò vi sóng, tủ lạnh, ấm điện, đồ thủy tinh, đồ sành sứ, dao kéo</li>
                                    <li>Phòng tắm được trang bị bồn tắm dài / tắm đứng và kính riêng</li>
                                    <li>Wi-fi</li>
                                    <li>Cơ sở vật chất phòng tắm và nhà bếp tiện nghi đến từ các nhãn hàng nổi tiếng như: Koller, Samsung, Electrolux...</li>
                                    <li>Căn hộ đều có tiện nghi sang trọng, điện thoại, kênh truyền hình cáp, TV màn hình phẳng, máy lạnh, khu vực phòng khách, máy giặt, tủ quần áo, giá treo quần áo, máy sấy tóc, phòng tắm, dép, vòi hoa sen, ghế sofa, sàn gỗ, tủ lạnh, lò vi sóng, đồ dùng nhà bếp, bàn ăn, khăn tắm, ga trải giường.</li>
                                <ul className="left-home-about-ul">Các dịch vụ khác</ul> 
                                    <li>Dịch vụ dọn phòng: 2 lần mỗi tuần</li>
                                    <li>Dịch vụ giặt đồ</li>
                                    <li>Bãi đỗ xe</li>
                                    <li>Camera an ninh 24/7</li>                                
                            </div>
                        </div>
                        <div className="home-title">
                            <h3 className="home-title__title">Tiện nghi chỗ ở</h3>
                            <span className="home-title__desc">Giới thiệu về các tiện nghi và dịch vụ tại nơi lưu trú</span>
                        </div>
                        <div className="home-sub-title">
                            <h4>Tiện ích</h4>      
                            <ul className='title-list'>
                                <li className='title-list__child'>
                                    <svg version="1.1" viewBox="0 0 24 24" className="svg-icon svg-fill" alt="" style={{width: '24px', height: '24px'}}><g fill="#000" fill-rule="nonzero"><path pid="0" d="M23.44 9.04a.502.502 0 01-.354-.146A14.9 14.9 0 0012.48 4.5 14.903 14.903 0 001.873 8.894a.5.5 0 01-.707-.707A15.896 15.896 0 0112.48 3.5c4.274 0 8.292 1.665 11.313 4.687a.5.5 0 01-.353.853z"></path><path pid="1" d="M20.612 11.868a.502.502 0 01-.354-.146c-4.289-4.288-11.268-4.288-15.557 0a.5.5 0 01-.707-.707c4.679-4.679 12.292-4.679 16.971 0a.5.5 0 01-.353.853z"></path><path pid="2" d="M17.783 14.697a.502.502 0 01-.354-.146 6.954 6.954 0 00-4.95-2.05c-1.87 0-3.627.728-4.95 2.05a.5.5 0 01-.707-.707 7.948 7.948 0 015.657-2.343c2.137 0 4.146.832 5.657 2.343a.5.5 0 01-.353.853zM12.48 20.5a2.503 2.503 0 01-2.5-2.5c0-1.378 1.122-2.5 2.5-2.5s2.5 1.122 2.5 2.5-1.122 2.5-2.5 2.5zm0-4c-.827 0-1.5.673-1.5 1.5s.673 1.5 1.5 1.5 1.5-.673 1.5-1.5-.673-1.5-1.5-1.5z"></path></g></svg>
                                    <span>Wifi</span>
                                </li>
                                <li className='title-list__child'>
                                    <svg version="1.1" viewBox="0 0 24 24" className="svg-icon svg-fill" alt="" style={{width: '24px', height: '24px'}}><path pid="0" fill="#000" fill-rule="nonzero" d="M16.293 14H8.707l-1.853 1.854a.5.5 0 01-.708-.708L7.293 14H3.5a.5.5 0 01-.5-.5v-11a.5.5 0 01.5-.5h18a.5.5 0 01.5.5v11a.5.5 0 01-.5.5h-3.793l1.147 1.146a.5.5 0 01-.708.708L16.293 14zM4 3v10h17V3H4zM3 21.5v1a.5.5 0 11-1 0V18a1.5 1.5 0 011.5-1.5h18A1.5 1.5 0 0123 18v4.5a.5.5 0 11-1 0v-1H3zm0-1h19V18a.5.5 0 00-.5-.5h-18a.5.5 0 00-.5.5v2.5zm8.5-1a.5.5 0 110-1h2a.5.5 0 110 1h-2zM5.854 5.854a.5.5 0 11-.708-.708l1-1a.5.5 0 11.708.708l-1 1zm0 3a.5.5 0 11-.708-.708l4-4a.5.5 0 11.708.708l-4 4z"></path></svg>
                                    <span>TV</span>
                                </li>
                                <li className='title-list__child'>
                                    <svg version="1.1" viewBox="0 0 24 24" className="svg-icon svg-fill" alt="" style={{width: '24px', height: '24px'}}><path pid="0" fill="#000" fill-rule="nonzero" d="M23 8.5h-2.085a1.5 1.5 0 01-2.83 0H1v4a.5.5 0 00.5.5h1v-.5A1.5 1.5 0 014 11h16a1.5 1.5 0 011.5 1.5v.5h1a.5.5 0 00.5-.5v-4zm0-1v-2a.5.5 0 00-.5-.5h-21a.5.5 0 00-.5.5v2h17.085a1.5 1.5 0 012.83 0H23zM20.5 13v-.5a.5.5 0 00-.5-.5H4a.5.5 0 00-.5.5v.5h17zm-19-9h21A1.5 1.5 0 0124 5.5v7a1.5 1.5 0 01-1.5 1.5h-21A1.5 1.5 0 010 12.5v-7A1.5 1.5 0 011.5 4zm5.886 16.182a.5.5 0 01-.772.636c-.817-.993-.817-2.086-.009-3.125.525-.674.525-1.248.009-1.875a.5.5 0 01.772-.636c.817.993.817 2.086.009 3.125-.525.674-.525 1.248-.009 1.875zm5 0a.5.5 0 01-.772.636c-.817-.993-.817-2.086-.009-3.125.525-.674.525-1.248.009-1.875a.5.5 0 01.772-.636c.817.993.817 2.086.009 3.125-.525.674-.525 1.248-.009 1.875zm5 0a.5.5 0 01-.772.636c-.817-.993-.817-2.086-.009-3.125.525-.674.525-1.248.009-1.875a.5.5 0 01.772-.636c.817.993.817 2.086.009 3.125-.525.674-.525 1.248-.009 1.875z"></path></svg>
                                    <span>Điều hoà</span>
                                </li>
                                <li className='title-list__child'>
                                    <svg version="1.1" viewBox="0 0 24 24" className="svg-icon svg-fill" alt="" style={{width: '24px', height: '24px'}}><path pid="0" fill="#000" fill-rule="nonzero" d="M15.07 11.21a4.5 4.5 0 100 6.58c-2.093-1.832-2.093-4.748 0-6.58zm.651.759c-1.628 1.432-1.628 3.63 0 5.062A4.48 4.48 0 0016.5 14.5a4.48 4.48 0 00-.779-2.531zM10 1H4.5a.5.5 0 00-.5.5V5h6V1zm1 0v4h9V1.5a.5.5 0 00-.5-.5H11zM4 6v17h16V6H4zm.5-6h15A1.5 1.5 0 0121 1.5v22a.5.5 0 01-.5.5h-17a.5.5 0 01-.5-.5v-22A1.5 1.5 0 014.5 0zM12 20a5.5 5.5 0 110-11 5.5 5.5 0 010 11zM6 3.5a.5.5 0 010-1h2a.5.5 0 010 1H6zM18 4a1 1 0 110-2 1 1 0 010 2zm-3 0a1 1 0 110-2 1 1 0 010 2z"></path></svg>
                                    <span>Máy giặt</span>
                                </li>
                                <li className='title-list__child'>
                                    <svg version="1.1" viewBox="0 0 24 24" className="svg-icon svg-fill" alt="" style={{width: '24px', height: '24px'}}><path pid="0" d="M7.381 0c-1.457 0-2.7 1.065-2.7 1.065a.48.48 0 10.6.75S6.438.96 7.381.96c1.226 0 2.28.855 2.28.855l.12.09V3.84H9.211a.48.48 0 00-.39.48v2.4h-.48a4.336 4.336 0 00-4.32 4.32v2.73a.492.492 0 000 .285V21.6a2.41 2.41 0 002.4 2.4h11.52a2.41 2.41 0 002.4-2.4v-7.59a.488.488 0 000-.195V11.04a4.336 4.336 0 00-4.32-4.32h-.48v-2.4a.48.48 0 00-.48-.48h-.48V1.92h.48a.483.483 0 00.422-.238.485.485 0 000-.484.483.483 0 00-.422-.238h-.855a.45.45 0 00-.15-.015.422.422 0 00-.045.015h-3.645a.45.45 0 00-.15-.015.422.422 0 00-.045.015h-.06C9.894.786 8.87 0 7.381 0zm3.36 1.92h2.88v1.92h-2.88V1.92zm-.96 2.88h4.8v1.92h-4.8V4.8zm-1.44 2.88h7.68c1.85 0 3.36 1.51 3.36 3.36v2.4h-5.805c.026-.154.045-.319.045-.48a2.888 2.888 0 00-2.88-2.88 2.888 2.888 0 00-2.88 2.88c0 .161.019.326.045.48H4.981v-2.4c0-1.85 1.51-3.36 3.36-3.36zm2.4 3.36c1.067 0 1.92.853 1.92 1.92s-.853 1.92-1.92 1.92a1.91 1.91 0 01-1.74-1.11v-.015l-.015-.03-.015-.015c-.002-.004.002-.011 0-.015l-.015-.015a1.944 1.944 0 01-.135-.72c0-1.067.853-1.92 1.92-1.92zm-5.76 3.36h3.27a2.89 2.89 0 002.49 1.44 2.89 2.89 0 002.49-1.44h6.15v7.2a1.44 1.44 0 01-1.44 1.44H6.421a1.44 1.44 0 01-1.44-1.44v-7.2zm10.08 1.44c-1.056 0-1.92.864-1.92 1.92s.864 1.92 1.92 1.92 1.92-.864 1.92-1.92-.864-1.92-1.92-1.92zm0 .96c.536 0 .96.424.96.96s-.424.96-.96.96a.952.952 0 01-.96-.96c0-.536.424-.96.96-.96zm-3.84 2.4c-.79 0-1.44.65-1.44 1.44 0 .79.65 1.44 1.44 1.44.79 0 1.44-.65 1.44-1.44 0-.79-.65-1.44-1.44-1.44zm0 .96c.27 0 .48.21.48.48s-.21.48-.48.48-.48-.21-.48-.48.21-.48.48-.48z" fill="#000" fill-rule="nonzero"></path></svg>
                                    <span>Dầu gội, dầu xả</span>
                                </li>
                                <li className='title-list__child'>
                                    <svg version="1.1" viewBox="0 0 24 24" className="svg-icon svg-fill" alt="" style={{width: '24px', height: '24px'}}><path pid="0" fill="#000" d="M0 7.429C0 3.367 1.964 0 4.571 0H19.43C22.036 0 24 3.367 24 7.429c0 4.06-1.964 7.428-4.571 7.428H16v8.572a.571.571 0 01-.571.571H.57a.571.571 0 01-.57-.571v-16zm14.857 7.428H4.571c-1.394 0-2.604-.963-3.428-2.48v10.48h13.714v-8zm4.572-1.143c1.81 0 3.428-2.772 3.428-6.285 0-3.514-1.617-6.286-3.428-6.286H7.035c1.286 1.323 2.108 3.66 2.108 6.286 0 2.626-.822 4.962-2.108 6.285H19.43zm-14.858 0C6.383 13.714 8 10.942 8 7.43c0-3.514-1.617-6.286-3.429-6.286-1.81 0-3.428 2.772-3.428 6.286 0 3.513 1.617 6.285 3.428 6.285zm0-4c-1.007 0-1.714-1.06-1.714-2.285 0-1.226.707-2.286 1.714-2.286 1.008 0 1.715 1.06 1.715 2.286 0 1.225-.707 2.285-1.715 2.285zm0-1.143c.256 0 .572-.474.572-1.142 0-.669-.316-1.143-.572-1.143-.255 0-.571.474-.571 1.143 0 .668.316 1.142.571 1.142z" fill-rule="evenodd"></path></svg>
                                    <span>Giấy vệ sinh</span>
                                </li>
                                <li className='title-list__child'>
                                    <svg version="1.1" viewBox="0 0 24 24" className="svg-icon svg-fill" alt="" style={{width: '24px', height: '24px'}}><g fill-rule="evenodd"><path pid="0" fill-rule="nonzero" d="M22.527 6.497h-.366v-.202c.107-1.298-.245-2.453-.993-3.254a3.18 3.18 0 00-2.443-1.024H3.945c-1.376 0-2.496 1.102-2.496 2.457v2.023h-.023C.64 6.497 0 7.127 0 7.901v.359c0 .774.64 1.403 1.426 1.403h.023v10.865c0 1.355 1.12 2.457 2.496 2.457h12.188c1.417 0 2.208-.28 2.646-.934.417-.624.421-1.51.421-2.537h1.757c.675 0 1.204-.67 1.204-1.524V9.663h.366c.787 0 1.426-.63 1.426-1.403V7.9c0-.773-.64-1.403-1.426-1.403zM1.45 8.742h-.023a.487.487 0 01-.49-.482V7.9c0-.265.22-.482.49-.482h.023v1.324zM21.225 6.36v.137H19.21c.043-.329.15-.593.314-.77a.773.773 0 01.577-.245c.39 0 .899.276 1.125.878zm-2.96 13.126c0 1.032-.019 1.686-.267 2.058-.162.242-.52.52-1.865.52H3.945c-.86 0-1.56-.69-1.56-1.536V18.05h15.88v1.435zm0-2.356H2.384v-1.98h15.88v1.98zm.572-12.028c-.307.327-.655.949-.573 2.057v7.07H2.384v-.88h4.578a.464.464 0 00.468-.46.465.465 0 00-.468-.461H2.385V4.474c0-.847.7-1.536 1.56-1.536h14.803a2.252 2.252 0 011.731.726c.298.32.513.72.638 1.18A2.001 2.001 0 0020.1 4.56c-.487 0-.935.192-1.263.541zm2.388 12.888c0 .396-.2.603-.268.603H19.2v-8.93h2.025v8.327zm1.793-9.73c0 .266-.22.482-.49.482H19.2V7.418h3.327c.27 0 .49.217.49.483v.359z"></path><path pid="1" d="M8.428 12.428h-.037a.464.464 0 00-.468.46c0 .255.21.461.468.461h.037a.464.464 0 00.468-.46.465.465 0 00-.468-.461z"></path></g></svg>
                                    <span>Khăn tắm</span>
                                </li>
                                <li className='title-list__child'>
                                    <svg version="1.1" viewBox="0 0 24 24" className="svg-icon svg-fill" alt="" style={{width: '24px', height: '24px'}}><path pid="0" d="M19.824 1.261L18.105 2.98l-3.273.399-.123.014-.096.068L2.745 12.99l-.014.014-.013.027-1.155 1.141-.303.317 8.25 8.25.317-.303 1.14-1.155.028-.014.014-.013 9.556-11.825.083-.097.014-.137.357-3.3 1.416-1.402.303-.317-2.915-2.915zm0 1.252L21.5 4.176l-.893.894-1.678-1.664.894-.894zm-1.692 1.36h.014l1.98 1.98v.014l-.234 2.324-4.056-4.056 2.296-.261zm-3.286.523l4.785 4.785-9.281 11.482-.027.04-.798.785-7.013-7.013.784-.797.042-.028 11.508-9.254zM12.11 8.824c-.555 0-1.102.235-1.526.66-.394.393-.518.844-.605 1.168-.047.17-.072.298-.138.413-.063.113-.136.23-.206.344-.345.548-.928 1.464-.206 2.186.178.179.399.3.632.344.045.233.164.45.344.632.22.22.52.344.839.344.488 0 .923-.282 1.347-.55.114-.07.23-.143.344-.206.115-.066.242-.091.412-.138.325-.087.776-.211 1.17-.605.905-.906.872-2.195-.07-3.135a2.62 2.62 0 00-.398-.33 2.62 2.62 0 00-.33-.399c-.47-.469-1.021-.728-1.609-.728zm0 .88c.337 0 .68.158.99.467.138.138.234.286.303.427.14.068.287.163.426.302.603.603.608 1.356.069 1.897-.392.392-.901.32-1.403.605-.455.26-.928.633-1.251.633a.303.303 0 01-.22-.082c-.155-.155-.143-.353.055-.55.273-.274.58-.686.825-1.046.093-.137-.028-.288-.165-.288a.148.148 0 00-.097.027c-.359.244-.773.552-1.044.825-.109.109-.217.165-.317.165a.332.332 0 01-.234-.11c-.287-.287.226-.897.55-1.471.286-.504.214-.999.606-1.389.262-.264.586-.412.907-.412zm-7.92 3.41l-.619.632.317.316.618-.632-.316-.316zm.646.646l-.632.619.316.316.632-.619-.316-.316zm.633.633l-.619.618.316.317.619-.62-.316-.316zm.632.632l-.619.633.317.316.619-.633-.317-.316zm.646.646l-.632.619.316.316.633-.619-.317-.316zm.633.633l-.619.619.316.316.62-.619-.317-.316zm.633.632l-.62.633.317.316.619-.633-.317-.316zm.646.647l-.633.618.316.317.633-.62-.316-.315zm.632.632l-.619.619.317.316.618-.619-.316-.316zm.633.633l-.619.632.316.316.619-.632-.316-.316zm.646.646l-.633.619.317.316.632-.619-.316-.316z" fill="#000" fill-rule="nonzero"></path></svg>
                                    <span>Kem đánh răng</span>
                                </li>
                                <li className='title-list__child'>
                                    <svg version="1.1" viewBox="0 0 24 24" className="svg-icon svg-fill" alt="" style={{width: '24px', height: '24px'}}><g fill-rule="evenodd"><path pid="0" d="M7.2 16.902a2.105 2.105 0 01-1.654-.57 1.804 1.804 0 01-.351-1.328.393.393 0 00-.331-.443.4.4 0 00-.458.315 2.576 2.576 0 00.526 1.96c.488.564 1.251.85 2.268.85.22 0 .4-.175.4-.392a.396.396 0 00-.4-.392zm2 0h-.4c-.22 0-.4.176-.4.392 0 .217.18.392.4.392h.4c.22 0 .4-.175.4-.392a.396.396 0 00-.4-.392zm7.6-5.49c.61-.059 1.215.15 1.654.569.288.381.414.858.351 1.329a.393.393 0 00.395.455.398.398 0 00.394-.328 2.573 2.573 0 00-.526-1.959c-.488-.564-1.251-.85-2.268-.85-.22 0-.4.175-.4.392 0 .216.18.392.4.392zm-1.2-.392a.396.396 0 00-.4-.393h-.4c-.22 0-.4.176-.4.393 0 .216.18.392.4.392h.4c.22 0 .4-.176.4-.392z"></path><path pid="1" fill-rule="nonzero" d="M2 9.451c1.104 0 2-.878 2-1.96 0-1.084-.896-1.962-2-1.962S0 6.407 0 7.49c.001 1.083.896 1.96 2 1.961zm0-3.137c.663 0 1.2.526 1.2 1.176 0 .65-.537 1.177-1.2 1.177-.663 0-1.2-.527-1.2-1.177S1.337 6.314 2 6.314zm8.8.392c0 .866.716 1.569 1.6 1.569.884 0 1.6-.703 1.6-1.57 0-.865-.716-1.568-1.6-1.568-.884 0-1.6.703-1.6 1.569zm2.4 0a.792.792 0 01-.8.784.792.792 0 01-.8-.784c0-.433.358-.784.8-.784.442 0 .8.35.8.784zM4.8 4.745c0 1.516 1.254 2.745 2.8 2.745 1.546 0 2.8-1.229 2.8-2.745S9.146 2 7.6 2c-1.546.002-2.798 1.23-2.8 2.745zm4.8 0c0 1.083-.896 1.96-2 1.96s-2-.877-2-1.96.896-1.96 2-1.96 1.999.878 2 1.96z"></path><path pid="2" d="M7.197 5.014a.722.722 0 01-.017-.54.39.39 0 00-.255-.495.402.402 0 00-.505.25c-.145.413-.1.867.127 1.243a1.22 1.22 0 001.053.45c.22 0 .4-.176.4-.393a.396.396 0 00-.4-.392.527.527 0 01-.403-.123z"></path><path pid="3" fill-rule="nonzero" d="M21.6 16.118a2.38 2.38 0 00-.431.042c.02-.144.03-.29.031-.435v-3.137c-.002-1.948-1.613-3.527-3.6-3.53H6.4c-1.987.003-3.598 1.582-3.6 3.53v3.137c.002 1.949 1.613 3.528 3.6 3.53h6.54a2.397 2.397 0 002.26 1.569c.349 0 .694-.076 1.008-.224A2.4 2.4 0 0018.4 22a2.4 2.4 0 002.192-1.4c.314.148.66.224 1.008.224 1.325 0 2.4-1.054 2.4-2.353 0-1.3-1.075-2.353-2.4-2.353zm-8.8 2.353H6.4c-1.546-.002-2.798-1.23-2.8-2.746v-3.137c.002-1.515 1.254-2.743 2.8-2.745h11.2c1.546.002 2.798 1.23 2.8 2.745v3.137c0 .086-.01.17-.018.253a2.409 2.409 0 00-2.196-1.028 2.39 2.39 0 00-1.978 1.39 2.375 2.375 0 00-1.008-.222c-1.325.001-2.399 1.054-2.4 2.353zm8.8 1.568a1.598 1.598 0 01-.988-.345.406.406 0 00-.387-.059.394.394 0 00-.253.294 1.588 1.588 0 01-1.57 1.286 1.588 1.588 0 01-1.57-1.286.395.395 0 00-.253-.294.406.406 0 00-.387.059c-.28.223-.63.345-.992.345-.884 0-1.6-.702-1.6-1.568 0-.867.716-1.569 1.6-1.569.36.001.708.123.988.345.11.085.256.107.387.06a.394.394 0 00.253-.295 1.588 1.588 0 011.57-1.285c.772 0 1.433.54 1.57 1.285a.395.395 0 00.253.294c.13.048.278.026.387-.059.28-.222.63-.344.992-.345.884 0 1.6.702 1.6 1.569 0 .866-.716 1.568-1.6 1.568z"></path></g></svg>
                                    <span>Xà phòng tắm</span>
                                </li>
                                <li className='title-list__child'>
                                    <svg version="1.1" viewBox="0 0 24 24" className="svg-icon svg-fill" alt="" style={{width: '24px', height: '24px'}}><g fill="none" fill-rule="evenodd"><path pid="0" d="M0 0h24v24H0z"></path><path pid="1" fill="#000" fill-rule="nonzero" d="M17.571 3.452L6.434 2.07a5.872 5.872 0 00-.669-.038C2.586 2.03 0 4.62 0 7.805v.12a5.78 5.78 0 002.932 5.028l1.54 6.226c.17.689.599 1.27 1.206 1.636a2.639 2.639 0 002.01.304l.042-.01a2.638 2.638 0 001.637-1.206 2.639 2.639 0 00.304-2.01l-1.112-4.495 9.012-1.12a.494.494 0 00.434-.49V3.943c0-.25-.186-.46-.434-.49zM8.711 18.13c.107.433.039.88-.191 1.262-.23.381-.595.65-1.027.757l-.043.01c-.432.107-.88.04-1.26-.19a1.656 1.656 0 01-.758-1.028l-1.36-5.495a5.737 5.737 0 002.367.215l1.131-.14 1.14 4.61zm8.305-6.78L6.32 12.68A4.786 4.786 0 01.99 7.925v-.12a4.786 4.786 0 015.328-4.754L17.016 4.38v6.97z"></path><path pid="2" fill="#000" fill-rule="nonzero" d="M2.763 7.865a3.293 3.293 0 003.29 3.29 3.293 3.293 0 003.288-3.29 3.293 3.293 0 00-3.289-3.289 3.293 3.293 0 00-3.289 3.29zm5.59 0c0 1.269-1.032 2.3-2.3 2.3a2.303 2.303 0 01-2.301-2.3c0-1.268 1.032-2.3 2.3-2.3 1.269 0 2.3 1.032 2.3 2.3z"></path><path pid="3" fill="#000" d="M19.196 6.305c.288 0 .426.155.688.476.289.356.684.843 1.455.843s1.166-.487 1.455-.843c.262-.321.4-.476.687-.476a.494.494 0 100-.99c-.77 0-1.166.487-1.455.843-.261.322-.4.477-.687.477-.288 0-.426-.155-.687-.477-.29-.356-.685-.842-1.455-.842a.494.494 0 000 .989zm4.285 1.998c-.77 0-1.166.486-1.455.842-.261.322-.4.477-.687.477-.288 0-.426-.155-.687-.477-.29-.356-.685-.842-1.455-.842a.494.494 0 100 .989c.287 0 .425.155.687.476.289.356.684.843 1.455.843s1.166-.487 1.455-.843c.262-.321.4-.476.688-.476a.494.494 0 100-.99z"></path></g></svg>
                                    <span>Máy sấy</span>
                                </li>
                                <li className='title-list__child'>
                                    <svg version="1.1" viewBox="0 0 24 19" className="svg-icon svg-fill" alt="Mạng gia đình" style={{width: '24px', height: '24px'}}><g fill="#000" fill-rule="evenodd"><path pid="0" d="M21.5 10.99h-4.141c-3.3 0-8.253.018-14.86.055C1.123 11.045 0 12.122 0 13.5v2.938c0 1.378 1.122 2.5 2.5 2.5h19c1.378 0 2.5-1.122 2.5-2.5-.002-1.04-.004-2.02-.004-2.938a2.51 2.51 0 00-2.496-2.51zm0 7.01h-19c-.827 0-1.5-.673-1.5-1.5v-3c0-.827.673-1.5 1.5-1.5h19c.827 0 1.5.673 1.5 1.5v3c0 .827-.673 1.5-1.5 1.5z" fill-rule="nonzero"></path><path pid="1" d="M17.5 4.261a.5.5 0 00-.5.5v7a.5.5 0 001 0v-7a.5.5 0 00-.5-.5z"></path><circle pid="2" cx="3.5" cy="14.761" r="1"></circle><circle pid="3" cx="6.5" cy="14.761" r="1"></circle><circle pid="4" cx="9.5" cy="14.761" r="1"></circle><circle pid="5" cx="12.5" cy="14.761" r="1"></circle><path pid="6" d="M20.5 14.261h-5a.5.5 0 000 1h5a.5.5 0 000-1zM16 4.761c0-.4.156-.777.439-1.06a.5.5 0 00-.707-.708A2.484 2.484 0 0015 4.761c0 .668.26 1.296.732 1.768a.502.502 0 00.707 0 .5.5 0 000-.707A1.492 1.492 0 0116 4.762zM19.268 2.993a.5.5 0 00-.707.707c.283.284.439.66.439 1.061 0 .401-.156.778-.44 1.061a.5.5 0 00.708.707c.472-.472.732-1.1.732-1.768 0-.668-.26-1.296-.732-1.768z"></path><path pid="7" d="M15.024 2.287a.5.5 0 00-.707-.707 4.506 4.506 0 000 6.364.502.502 0 00.707 0 .5.5 0 000-.707 3.505 3.505 0 010-4.95zM20.682 1.58a.5.5 0 00-.707.707 3.505 3.505 0 010 4.95.5.5 0 00.707.707 4.506 4.506 0 000-6.364z"></path><path pid="8" d="M13.61.873a.5.5 0 00-.707-.707 6.508 6.508 0 000 9.192.502.502 0 00.707 0 .5.5 0 000-.707 5.505 5.505 0 010-7.778zM22.096.166a.5.5 0 00-.707.707 5.505 5.505 0 010 7.778.5.5 0 00.707.707 6.508 6.508 0 000-9.192z"></path></g></svg>
                                    <span>Internet</span>
                                </li>
                            </ul>                      
                        </div>
                        <div className="home-sub-title">
                            <h4>Tiện ích bếp</h4>  
                            <ul className='title-list'>
                                <li className='title-list__child'>
                                    <svg version="1.1" viewBox="0 0 24 24" className="svg-icon svg-fill" alt="" style={{width: '24px', height: '24px'}}><g fill-rule="evenodd" transform="translate(1)"><path pid="0" d="M2 0h18a2 2 0 012 2v20a2 2 0 01-2 2H2a2 2 0 01-2-2V2a2 2 0 012-2zm1 1a2 2 0 00-2 2v18a2 2 0 002 2h16a2 2 0 002-2V3a2 2 0 00-2-2H3z"></path><path pid="1" d="M11 19a8 8 0 110-16 8 8 0 010 16zm0-1a7 7 0 100-14 7 7 0 000 14z"></path><rect pid="2" width="5" height="1" x="14" y="21" rx=".5"></rect><rect pid="3" width="2" height="1" x="3" y="21" rx=".5"></rect><rect pid="4" width="2" height="1" x="6" y="21" rx=".5"></rect></g></svg>
                                    <span>Bếp điện</span>                                    
                                </li>
                                <li className='title-list__child'>
                                    <svg version="1.1" viewBox="0 0 24 24" className="svg-icon svg-fill" alt="" style={{width: '24px', height: '24px'}}><path pid="0" fill="#000" fill-rule="nonzero" d="M17.5 0A1.5 1.5 0 0119 1.5v21a1.5 1.5 0 01-1.5 1.5h-11A1.5 1.5 0 015 22.5v-21A1.5 1.5 0 016.5 0h11zM6 13h12V1.5a.5.5 0 00-.5-.5h-11a.5.5 0 00-.5.5V13zm0 1v8.5a.5.5 0 00.5.5h11a.5.5 0 00.5-.5V14H6zm1-6.5a.5.5 0 011 0v4a.5.5 0 11-1 0v-4zm0 8a.5.5 0 111 0v4a.5.5 0 11-1 0v-4zm8.854 6.354a.5.5 0 01-.708-.708l1-1a.5.5 0 01.708.708l-1 1zm-3 0a.5.5 0 01-.708-.708l4-4a.5.5 0 01.708.708l-4 4z"></path></svg>
                                    <span>Tủ lạnh</span>
                                </li>                                
                            </ul>                          
                        </div>
                        <div className="home-sub-title">
                            <h4>Tiện ích phòng</h4>  
                            <ul className='title-list'>
                                <li className='title-list__child'>
                                    <svg version="1.1" viewBox="0 0 24 24" className="svg-icon svg-fill" alt="" style={{width: '24px', height: '24px'}}><g fill-rule="evenodd"><path pid="0" fill-rule="nonzero" d="M23.485 13.987H18.2V6.87h.257a.468.468 0 000-.935h-.264A6.23 6.23 0 0011.977 0 6.23 6.23 0 005.76 5.936h-.264a.468.468 0 000 .935h.257v7.116H.468a.468.468 0 00-.468.468v9.03c0 .259.21.468.468.468h23.017c.259 0 .468-.21.468-.468v-9.03a.468.468 0 00-.468-.468zm-5.286.936h1.941v5.287H18.2v-5.287zM12.444.957a5.294 5.294 0 014.811 4.979h-1.608a3.708 3.708 0 00-3.203-3.203V.957zm0 2.721a2.772 2.772 0 012.258 2.258h-2.258V3.678zm0 5.543V6.87h3.269l-.89.89a.468.468 0 00.662.662l1.551-1.552h.227v7.116h-4.157l3.266-3.266a.468.468 0 00-.662-.661l-3.266 3.265V9.221zm4.82 5.702v5.287h-1.942v-5.287h1.941zm-4.82 0h1.942v5.287h-1.942v-5.287zM11.51.957v1.776a3.708 3.708 0 00-3.203 3.203H6.698a5.295 5.295 0 014.81-4.98zm0 2.721v2.258H9.25a2.772 2.772 0 012.258-2.258zm-4.82 8.427l1.551-1.552a.468.468 0 00-.661-.661l-.89.89v-3.91h4.819v2.155L9.25 11.285a.468.468 0 00.662.661l1.596-1.596v3.637H6.69v-1.882zm4.819 2.818v5.287H9.567v-5.287h1.942zm-4.819 0h1.942v5.287H6.69v-5.287zm-2.877 0h1.941v5.287H3.813v-5.287zm-2.877 0h1.941v5.287H.936v-5.287zm0 8.095v-1.872h22.082v1.872H.936zm22.082-2.808h-1.942v-5.287h1.942v5.287z"></path><path pid="1" d="M9.202 9.593a.468.468 0 00-.666-.658l-.008.008a.468.468 0 00.666.658l.008-.008zm4.671-.882l-.008.008a.468.468 0 00.661.662l.008-.008a.468.468 0 00-.661-.662z"></path></g></svg>
                                    <span>Ban Công</span>
                                </li>
                                                            
                            </ul>                           
                        </div>
                        <div className="home-title">
                            <h3 className="home-title__title">Giá phòng</h3>
                            <span className="home-title__desc">Giá có thể tăng vào cuối tuần hoặc ngày lễ</span>
                            <div className="home-price">
                                <div className="home-price__child">
                                    <span className="home-price__child-left">Thứ hai - Thứ năm</span>
                                    <span className="home-price__child-right">850,000₫</span>
                                </div>
                                <div className="home-price__child">
                                    <span className="home-price__child-left">Thứ sáu - Chủ nhật</span>
                                    <span className="home-price__child-right">900,000₫</span>
                                </div>
                                <div className="home-price__child">
                                    <span className="home-price__child-left">Phí trẻ em tăng thêm</span>
                                    <span className="home-price__child-right">125,000₫ (sau 2 khách)</span>
                                </div>
                                <div className="home-price__child">
                                    <span className="home-price__child-left">Thuê theo tháng</span>
                                    <span className="home-price__child-right">-7.88 %</span>
                                </div>
                                <div className="home-price__child">
                                    <span className="home-price__child-left">Số đêm tối thiểu</span>
                                    <span className="home-price__child-right">1 đêm</span>
                                </div>
                                <div className="home-price__child">
                                    <span className="home-price__child-left">Số đêm tối đa</span>
                                    <span className="home-price__child-right">90 đêm</span>
                                </div>
                            </div>                            
                        </div>
                        <div className="home-title">
                            <h3 className="home-title__title">Availability</h3>
                            <span className="home-title__desc">Giá có thể tăng vào cuối tuần hoặc ngày lễ</span>
                            <div className="book-home__date">
                                <div className="header__field-icon-box-left book-home-date-header__field-icon-box-left">
                                    <i className="header__field-month-icon <sm-gutter-left fas fa-chevron-left sm-gutter-left"></i>
                                </div>
                                <div className="header__field-icon-box-right book-home-date-header__field-icon-box-right">
                                    <i className="header__field-month-icon fas fa-chevron-right sm-gutter-right"></i>
                                </div>
                                <div className="header__field-left-month book-home-header__field-month">
                                    <div className="header__field-label">
                                        <h4 className="header__field-month-label">August 2021</h4>
                                    </div>
                                    <div className="header__field-weekend">
                                        <div className="header__field-weekend-list">
                                            <div className="header__field-weekend-item">Mon</div>
                                            <div className="header__field-weekend-item">Tus</div>
                                            <div className="header__field-weekend-item">Wed</div>
                                            <div className="header__field-weekend-item">Thu</div>
                                            <div className="header__field-weekend-item">Fri</div>
                                            <div className="header__field-weekend-item">Sat</div>
                                            <div className="header__field-weekend-item">Sun</div>
                                        </div>
                                    </div>
                                    <div className="header__field-month-table">
                                        <ul className="header__field-month-week">
                                            <li className="header__field-month-day header__field-month-day--empty"></li>
                                            <li className="header__field-month-day header__field-month-day--empty"></li>
                                            <li className="header__field-month-day header__field-month-day--empty"></li>
                                            <li className="header__field-month-day header__field-month-day--empty"></li>
                                            <li className="header__field-month-day header__field-month-day--empty"></li>
                                            <li className="header__field-month-day header__field-month-day--empty"></li>
                                            <li className="header__field-month-day header__field-month-day--old">1</li>
                                        </ul>
                                        <ul className="header__field-month-week">
                                            <li className="header__field-month-day header__field-month-day--old">2</li>
                                            <li className="header__field-month-day header__field-month-day--old">3</li>
                                            <li className="header__field-month-day header__field-month-day--old">4</li>
                                            <li className="header__field-month-day header__field-month-day--old">5</li>
                                            <li className="header__field-month-day header__field-month-day--old">6</li>
                                            <li className="header__field-month-day header__field-month-day--old">7</li>
                                            <li className="header__field-month-day header__field-month-day--old">8</li>
                                        </ul>
                                        <ul className="header__field-month-week">
                                            <li className="header__field-month-day header__field-month-day--old">9</li>
                                            <li className="header__field-month-day header__field-month-day--old">10</li>
                                            <li className="header__field-month-day header__field-month-day--old">11</li>
                                            <li className="header__field-month-day header__field-month-day--old">12</li>
                                            <li className="header__field-month-day header__field-month-day--old">13</li>
                                            <li className="header__field-month-day header__field-month-day--old">14</li>
                                            <li className="header__field-month-day header__field-month-day--old">15</li>
                                        </ul>
                                        <ul className="header__field-month-week">
                                            <li className="header__field-month-day header__field-month-day--old">16</li>
                                            <li className="header__field-month-day header__field-month-day--old">17</li>
                                            <li className="header__field-month-day header__field-month-day--old">18</li>
                                            <li className="header__field-month-day header__field-month-day--old">19</li>
                                            <li className="header__field-month-day header__field-month-day--old">20</li>
                                            <li className="header__field-month-day header__field-month-day--old">21</li>
                                            <li className="header__field-month-day header__field-month-day--active">22</li>
                                        </ul>
                                        <ul className="header__field-month-week">
                                            <li className="header__field-month-day header__field-month-day--new">23</li>
                                            <li className="header__field-month-day header__field-month-day--new">24</li>
                                            <li className="header__field-month-day header__field-month-day--new">25</li>
                                            <li className="header__field-month-day header__field-month-day--new">26</li>
                                            <li className="header__field-month-day header__field-month-day--new">27</li>
                                            <li className="header__field-month-day header__field-month-day--new">28</li>
                                            <li className="header__field-month-day header__field-month-day--new">29</li>
                                        </ul>
                                        <ul className="header__field-month-week">
                                            <li className="header__field-month-day header__field-month-day--new">30</li>
                                            <li className="header__field-month-day header__field-month-day--new">31</li>
                                            <li className="header__field-month-day header__field-month-day--empty"></li>
                                            <li className="header__field-month-day header__field-month-day--empty"></li>
                                            <li className="header__field-month-day header__field-month-day--empty"></li>
                                            <li className="header__field-month-day header__field-month-day--empty"></li>
                                            <li className="header__field-month-day header__field-month-day--empty"></li>
                                        </ul>
                                    </div>
                                    <button className="btn__remove" style={{color: 'var(--hover-color)'}}>Xóa</button>

                                </div>
                                <div className="header__field-right-month book-home-header__field-month">
                                    <div className="header__field-label">
                                        <h4 className="header__field-month-label">September 2021</h4>
                                    </div>
                                    <div className="header__field-weekend">
                                        <div className="header__field-weekend-list">
                                            <div className="header__field-weekend-item">Mon</div>
                                            <div className="header__field-weekend-item">Tus</div>
                                            <div className="header__field-weekend-item">Wed</div>
                                            <div className="header__field-weekend-item">Thu</div>
                                            <div className="header__field-weekend-item">Fri</div>
                                            <div className="header__field-weekend-item">Sat</div>
                                            <div className="header__field-weekend-item">Sun</div>
                                        </div>
                                    </div>
                                    <div className="header__field-month-table">
                                        <ul className="header__field-month-week">
                                            <li className="header__field-month-day header__field-month-day--empty"></li>
                                            <li className="header__field-month-day header__field-month-day--empty"></li>
                                            <li className="header__field-month-day header__field-month-day--new">1</li>
                                            <li className="header__field-month-day header__field-month-day--new">2</li>
                                            <li className="header__field-month-day header__field-month-day--new">3</li>
                                            <li className="header__field-month-day header__field-month-day--new">4</li>
                                            <li className="header__field-month-day header__field-month-day--new">5</li>
                                        </ul>
                                        <ul className="header__field-month-week">
                                            <li className="header__field-month-day header__field-month-day--new">6</li>
                                            <li className="header__field-month-day header__field-month-day--new">7</li>
                                            <li className="header__field-month-day header__field-month-day--new">8</li>
                                            <li className="header__field-month-day header__field-month-day--new">9</li>
                                            <li className="header__field-month-day header__field-month-day--new">10</li>
                                            <li className="header__field-month-day header__field-month-day--new">11</li>
                                            <li className="header__field-month-day header__field-month-day--new">12</li>
                                        </ul>
                                        <ul className="header__field-month-week">
                                            <li className="header__field-month-day header__field-month-day--new">13</li>
                                            <li className="header__field-month-day header__field-month-day--new">14</li>
                                            <li className="header__field-month-day header__field-month-day--new">15</li>
                                            <li className="header__field-month-day header__field-month-day--new">16</li>
                                            <li className="header__field-month-day header__field-month-day--new">17</li>
                                            <li className="header__field-month-day header__field-month-day--new">18</li>
                                            <li className="header__field-month-day header__field-month-day--new">19</li>
                                        </ul>
                                        <ul className="header__field-month-week">
                                            <li className="header__field-month-day header__field-month-day--new">20</li>
                                            <li className="header__field-month-day header__field-month-day--new">21</li>
                                            <li className="header__field-month-day header__field-month-day--new">22</li>
                                            <li className="header__field-month-day header__field-month-day--new">23</li>
                                            <li className="header__field-month-day header__field-month-day--new">24</li>
                                            <li className="header__field-month-day header__field-month-day--new">25</li>
                                            <li className="header__field-month-day header__field-month-day--new">26</li>
                                        </ul>
                                        <ul className="header__field-month-week">
                                            <li className="header__field-month-day header__field-month-day--new">27</li>
                                            <li className="header__field-month-day header__field-month-day--new">28</li>
                                            <li className="header__field-month-day header__field-month-day--new">29</li>
                                            <li className="header__field-month-day header__field-month-day--new">30</li>
                                            <li className="header__field-month-day header__field-month-day--new">31</li>
                                            <li className="header__field-month-day header__field-month-day--empty"></li>
                                            <li className="header__field-month-day header__field-month-day--empty"></li>
                                        </ul>
                                        <ul className="header__field-month-week">
                                            <li className="header__field-month-day header__field-month-day--empty"></li>
                                            <li className="header__field-month-day header__field-month-day--empty"></li>
                                            <li className="header__field-month-day header__field-month-day--empty"></li>
                                            <li className="header__field-month-day header__field-month-day--empty"></li>
                                            <li className="header__field-month-day header__field-month-day--empty"></li>
                                            <li className="header__field-month-day header__field-month-day--empty"></li>
                                            <li className="header__field-month-day header__field-month-day--empty"></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="home-title">
                            <h3 className="home-title__title">Nội quy và chính sách về chỗ ở</h3>
                            <div className="home-sub-title">
                                <h4>Chính sách hủy phòng</h4>
                                <div className="room-des">
                                    <b>Linh hoạt: </b>
                                    <span> Miễn phí hủy phòng trong vòng 48h sau khi đặt phòng thành công và trước 1 ngày so với thời gian check-in. Sau đó, hủy phòng trước 1 ngày so với thời gian check-in, được hoàn lại 100% tổng số tiền đã trả (trừ phí dịch vụ).</span>
                                    <span style={{color: 'var(--hover-color)'}}> Chi tiết</span>
                                    <div className="cancellation-rule" style={{maxWidth: '678px'}}>
                                        <div className="cancellation-rule__title">
                                            <div className="d-inline-flex" style={{width: '169px', alignItems: 'center'}}>
                                                <p className="d-inline-block rule-title">Đặt phòng thành công</p>
                                            </div>
                                            <div className="d-inline-flex" style={{width: '169px', alignItems: 'center'}}>
                                                <p className="d-inline-block rule-title" style={{textAlign: 'center'}}> Sau 48h</p>
                                            </div>
                                            <div className="d-inline-flex" style={{width: '169px', alignItems: 'center'}}>
                                                <p className="d-inline-block rule-title" style={{textAlign: 'center'}}>1 ngày trước check-in</p>
                                            </div>
                                            <div className="d-inline-flex" style={{width: '169px', alignItems: 'center', justifyContent: 'flex-end'}}>
                                                <p className="d-inline-block rule-title">Check-in</p>
                                            </div>
                                        </div>
                                        <div className="cancellation-rule__content">
                                            <div className="d-inline-block">
                                                <div className="rule-process" style={{width: '169px'}}>
                                                    <div className="rule-process__symbol is-first" style={{width: '32px', height: '32px', background:'rgb(40, 202, 110)'}}>
                                                        <div className="check-mark"></div>
                                                        <div className="rule-process__line is-top" style={{width: '169px', height: '4px', background:'rgb(40, 202, 110)'}}></div> 
                                                        <div className="rule-process__line is-bottom" style={{width: '1px', height: '120px', background:' rgb(40, 202, 110)'}}></div>
                                                    </div> 
                                                    <p className="mb-0 mt--6 rule-process__txt p--small" style={{marginLeft: '36px'}}>Hoàn tiền 100%</p>
                                                </div>
                                            </div>
                                            <div className="d-inline-block">                                                
                                                <div className="rule-process" style={{width: '169px'}}>
                                                    <div className="rule-process__symbol is-normal" style={{width: '20px', height: '20px', marginTop: '6px', background:'rgb(255, 181, 0)'}}>
                                                        <div className="rule-process__line is-top" style={{transform: 'translateY(10px)', width: '169px', height: '4px', background:'rgb(255, 181, 0)'}}></div> 
                                                        <div className="rule-process__line is-bottom" style={{ransform: 'translate(10px, 10px)', width: '1px', height: '120px', background:'rgb(255, 181, 0)'}}></div>
                                                    </div> 
                                                    <p className="mb-0 mt--6 rule-process__txt p--small" style={{marginLeft: '28px'}}>Hoàn tiền 100% (trừ phí dịch vụ)</p>
                                                </div>
                                            </div>
                                            <div className="d-inline-block" style={{flexGrow: 1}}>
                                                <div className="rule-process">
                                                    <div className="rule-process__symbol is-normal" style={{width: '20px', height: '20px', marginTop: '6px', background:'rgb(246, 94, 57)'}}>
                                                        <div className="rule-process__line is-top" style={{transform: 'translate(10px, 10px)', width: '280px', height: '4px', background:' rgb(246, 94, 57)'}}></div> 
                                                        <div className="rule-process__line is-bottom" style={{transform: 'translate(10px, 15px)',width: '1px', height: '120px', background:' rgb(246, 94, 57)'}}></div>
                                                    </div> 
                                                    <p className="mb-0 mt--6 rule-process__txt p--small" style={{marginLeft: '28px'}}>Hoàn 50% (trừ phí đêm đầu tiên, phí dịch vụ và các phí kèm theo)</p>
                                                    </div>
                                                </div>
                                                <div className="d-inline-block">
                                                    <div className="rule-process is-last">
                                                        <div className="rule-process__symbol is-last" style={{transform: 'translate(-20px, 0px)',width: '32px', height: '32px', background:' rgb(246, 94, 57)'}}>
                                                            <svg version="1.1" viewBox="0 0 24 24" className="symbol-icon svg-icon svg-fill" style={{width: '16px', height: '16px', transform: 'translate(8px, 6px)'}}>
                                                                <g fill="none" fill-rule="evenodd">
                                                                    <path pid="0" d="M0 0h24v24H0z"></path>
                                                                    <path pid="1" fill="#FFF" d="M3.957 8.415L11.48 3.82a1 1 0 011.042 0l7.522 4.596A2 2 0 0121 10.122V19a2 2 0 01-2 2H5a2 2 0 01-2-2v-8.878a2 2 0 01.957-1.707zM13.8 12c-.644 0-1.397.73-1.8 1.2-.403-.47-1.156-1.2-1.8-1.2-1.14 0-1.8.889-1.8 2.02 0 1.253 1.2 2.58 3.6 3.98 2.4-1.4 3.6-2.7 3.6-3.9 0-1.131-.66-2.1-1.8-2.1z"></path>
                                                                </g>
                                                            </svg> 
                                                            <div className="rule-process__line is-top" style={{transform: 'translate(25px, -2px)', width: '169px', height: '4px', background:' rgb(246, 94, 57)'}}></div> 
                                                            <div className="rule-process__line is-bottom" style={{transform: 'translate(15px, 10px)', width: '1px', height: '120px', background:' rgb(246, 94, 57)'}}></div>
                                                        </div> 
                                                            <p className="mb-0 mt--6 rule-process__txt p--small" style={{marginLeft: '36px'}}></p>
                                                        </div>
                                                    </div>
                                                </div>
                                    </div>
                                    
                                </div>
                            </div>    
                            <div className="home-sub-title left-home-about">
                                <h4 style={{marginBottom: '10px'}}>Lưu ý đặc biệt</h4> 
                                <ul  className="left-home-about-ul">
                                    <li>Đưa đón sân bay: giá đưa đón từ sân bay Nội Bài tới căn hộ The Galaxy Home 400.000VND / chiều / xe 4 chỗ</li>
                                    <li>Không hút thuốc trong căn hộ</li>
                                    <li>Không sử dụng chất kích thích</li>
                                    <li>Không mở tiệc trong căn hộ</li>
                                    <li>Không mang theo vật nuôi</li>
                                    <li>Vui lòng giữ im lặng sau 22h</li>
                                    <li>Vui lòng tắt các thiết bị khi bạn ra khỏi phòng</li>
                                </ul>   
                            </div>   
                            <div className="home-sub-title left-home-about">
                                <h4 style={{marginBottom: '10px'}}>Thời gian nhận phòng</h4> 
                                <div className="home-price">
                                    <div className="home-price__child">
                                        <span className="home-price__child-left">Nhận phòng</span>
                                        <span className="home-price__child-right">02:00 pm</span>
                                    </div>
                                    <div className="home-price__child">
                                        <span className="home-price__child-left">Trả phòng</span>
                                        <span className="home-price__child-right">12:00 pm</span>
                                    </div>
                                </div>
                            </div>                        
                        </div>                        
                    </div>
                    <div className="book-home-right col l-4">
                        <div className="right-top-share">
                            <span>Chia sẻ</span>
                            <i className="fas fa-upload"></i>
                        </div>

                        <div className="right-sidebar">
                            <div className="book-home-sidebar__content">
                                <div className="book-home-sidebar__price">
                                    <span className="book-home-sidebar__bold">{numberCusts * 800000}</span><span style={{fontSize:'14px'}}>vnđ</span>
                                    {/* <span className="book-home-sidebar__notbold">/đêm</span> */}
                                </div>
                                <div className="book-home-sidebar__time">
                                    <input type="date" id="bookingtime" name="bookingtime" required />
                                    <span>đến</span>
                                    <input type="date" id="bookingtime" name="bookingtime" required />
                                </div>
                                <div className="header__field header__field-member cust-border">
                                    <div className="header__field-wrapper room-header__field-wrapper" onClick={setBlockCust}>
                                        <span className="header__field-label js-header__field-label-member" style={{fontSize:'18px'}}>{numberCusts < 2 ? 1 : numberCusts} khách</span>
                                    </div>

                                    {/* SET NUMBER */}
                                    <div className={blockState.cust ? "header__field-set-number block" : "header__field-set-number"}>
                                        <div className="header__field-label-container room-header__field-label-container">
                                            <div className="">
                                                <div className="set__number-select">
                                                    <h4 className="set__number-select-label">Người lớn</h4>
                                                    <div className="set__number-select-option">
                                                    <div className={blockState.custNumber1 <= 1 ? "set__number-select-up-and-down set__number-select-up-and-down--disabled" : "set__number-select-up-and-down set__number-select-up-and-down--enabled"} onClick={subCust1}>
                                                        <i className="set__number-select-icon far fa-minus"></i>
                                                    </div>
                                                        <span className="set__number-select-number">{blockState.custNumber1}</span>
                                                    <div className="set__number-select-up-and-down set__number-select-up-and-down--enabled" onClick={plusCust1}>
                                                        <i className="set__number-select-icon far fa-plus"></i>
                                                    </div>
                                                    </div>
                                                </div>
                                                <div className="set__number-select mt-16">
                                                    <div className="set__number-title">
                                                        <h4 className="set__number-select-label">Trẻ em</h4>
                                                        <p className="set__number-select-desc">Tuổi từ 2-12 tuổi</p>
                                                    </div>
                                                    <div className="set__number-select-option">
                                                        <div className={blockState.custNumber2 <= 0 ? "set__number-select-up-and-down set__number-select-up-and-down--disabled" : "set__number-select-up-and-down set__number-select-up-and-down--enabled"} onClick={subCust2}>
                                                            <i className="set__number-select-icon far fa-minus"></i>
                                                        </div>
                                                        <span className="set__number-select-number">{blockState.custNumber2}</span>
                                                        <div className="set__number-select-up-and-down set__number-select-up-and-down--enabled" onClick={plusCust2}>
                                                            <i className="set__number-select-icon far fa-plus"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="set__number-select mt-16">
                                                    <div className="set__number-title">
                                                        <h4 className="set__number-select-label">Trẻ sơ sinh</h4>
                                                        <p className="set__number-select-desc">Dưới 2 tuổi</p>
                                                    </div>
                                                    <div className="set__number-select-option">
                                                        <div className={blockState.custNumber3 <= 0 ? "set__number-select-up-and-down set__number-select-up-and-down--disabled" : "set__number-select-up-and-down set__number-select-up-and-down--enabled"} onClick={subCust3}>
                                                            <i className="set__number-select-icon far fa-minus"></i>
                                                        </div>
                                                        <span className="set__number-select-number">{blockState.custNumber3}</span>
                                                        <div className="set__number-select-up-and-down set__number-select-up-and-down--enabled" onClick={plusCust3}>
                                                            <i className="set__number-select-icon far fa-plus"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div className="set__number-footer">
                                                    <button className="btn__remove none-margin" onClick={setBlockCustDelete}>Xóa</button>
                                                    <button className="btn__apply none-margin" onClick={setBlockCust}>Áp dụng</button>
        
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {/* END: SET NUMBER */}
                                </div>
                                <Link to='/locations/book-home/info-cast' style={{textDecoration:'none'}}>
                                    <div className="book-home-sidebar__time" style={{padding: '0'}}>
                                        <button type="submit" className="acount-btn">  
                                            <svg version="1.1" viewBox="0 0 20 20" className="mr--6 svg-icon svg-fill" style={{width: '20px', height: '20px'}}>
                                                <g fill="none" fill-rule="evenodd" transform="translate(-2 -2)">
                                                    <path pid="0" d="M0 0h24v24H0z"></path>
                                                    <circle pid="1" cx="12" cy="12" r="10" fill="#FFF" opacity=".3"></circle>
                                                    <path pid="2" fill="#FFF" d="M12.42 17.158l3.037-6.073a.75.75 0 00-.67-1.085H12V7.177a.75.75 0 00-1.42-.335l-3.037 6.073A.75.75 0 008.213 14H11v2.823a.75.75 0 001.42.335z"></path>
                                                </g>
                                            </svg>
                                            <span>Đặt ngay</span>
                                        </button>
                                    </div>
                                </Link>
                            </div>
                            <div className="book-home-sidebar__content">
                                <div className="book-home-sidebar__advise">
                                    <div className="lux-advise">
                                        <h4 className="lux-advise__title">Tư vấn từ Luxstay</h4>
                                        <span>Vui lòng cung cấp số điện thoại để nhận được tư vấn từ Luxstay cho chuyến đi của bạn.</span>
                                    </div>
                                    <div className="book-home-sidebar__time">
                                        <input type="text" name="" id="" placeholder="Tên khách hàng" />
                                    </div>

                                    <div className="cust-phone-number">
                                        <div className="book-home-sidebar__time w-140 m-r-10">
                                            <img src="../../img/book/vietnam.svg" alt="" style={{height:'20px', width:'24px'}} />
                                            <span className="book-home-sidebar__time-span">
                                                +84
                                                <i className="fas fa-sort-down m-l-4"></i>
                                            </span>
                                        </div>
                                        <div className="book-home-sidebar__time">
                                            <input type="text" name="" id="" placeholder="Số điện thoại" />
                                        </div>
                                    </div>

                                    <div className="book-home-sidebar__time" style={{padding: '0'}}>
                                        <button type="submit" className="acount-btn" style={{backgroundColor: 'black', backgroundImage: 'none', boxShadow: 'none'}}>
                                            <span>Nhận tư vấn miễn phí</span>
                                        </button>
                                    </div>

                                    <div className="book-home-sidebar__time support-phone-brg">
                                        <div className="support-phone-number">
                                            Gọi
                                            <b> 18006586 (miễn phí) </b>
                                            để được hỗ trợ.
                                        </div>
                                        <i className="far fa-comment-alt"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>               

                    
                </div>
                
            </div>
            <div className="home-same row-143 col">
                <div className="home-title">
                    <h3 className="home-title__title">Chỗ ở tương tự</h3>
                    <div className="body-locations-home mlr-8">
                        {locationData.map((homeInfo,index) => {
                            return (
                                <div className="l-2-4">                              
                                    <div className="home-wrapprer col-pad-0-8">
                                        <Link to='/locations/book-home'>
                                            <img className="home-img" src={homeInfo.src} alt="" />
                                            <div className="home-decs">
                                                <p>{homeInfo.decs}</p>
                                                <div className="star">
                                                    <i className="fas fa-star" style={{color: '#ffb025'}}></i>
                                                    <p style={{color: 'black'}}>{homeInfo.numberStar}</p>
                                                    <p>({homeInfo.personStar})</p>
                                                </div>
                                            </div>
                                            <div className="home-label">
                                                <svg data-v-ebd93bc0="" version="1.1" viewBox="0 0 20 20" className="is-instant-book mr--6 svg-icon svg-fill" style={{width: '20px', height: '20px'}}><title>Đặt phòng ngay, không cần đợi chủ nhà phê duyệt</title><defs><linearGradient x1="100%" y1="50%" x2="0%" y2="50%" id="svgicon_instant_book_a"><stop stop-color="#F68A39" offset="0%"></stop><stop stop-color="#F65E39" offset="100%"></stop></linearGradient></defs><g fill="none" fill-rule="evenodd"><circle pid="0" fill="url(#svgicon_instant_book_a)" cx="10" cy="10" r="10"></circle><path pid="1" d="M10.42 15.158l3.037-6.073A.75.75 0 0012.787 8H10V5.177a.75.75 0 00-1.42-.335l-3.037 6.073A.75.75 0 006.213 12H9v2.823a.75.75 0 001.42.335z" fill="#FFF"></path></g></svg>
                                                <p>{homeInfo.label}</p>
                                            </div>
                                            <b className="home-price">{homeInfo.price}</b>
                                        </Link>
                                    </div>                                    
                                </div>                                        
                            )
                        })                        
                        }
                    </div>
                </div>
            </div>
                
            <div className="" style={{borderBottom: '1px solid var(--border-color)', marginTop: '48px'}}></div>
            <FooterBottom />
        </>
    )
}

export default BookHome

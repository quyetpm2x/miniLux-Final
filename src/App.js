import Home from './home/zhome';
import SignIn from './sign/signIn';
import SignUp from './sign/signUp';
import { BrowserRouter, Route } from 'react-router-dom';
import Locations from './locations/Locations';
import BookHome from './book/bookHome';
import {LabelContext} from './context';
import { HeightLightPlaceData } from './home/heightLightPlaceData';
import InfoCast from './infoCastAndAccept/infoCast';
import QRcode from './QRcode/QRcode';




function App() {
  return (
    <BrowserRouter>
      <LabelContext.Provider value={HeightLightPlaceData}>
        <Route path='/' component={Home} exact/>
        <Route path='/locations' exact component={Locations }  />
        <Route path='/signin' component={SignIn} exact />
        <Route path='/signup' component={SignUp} exact />
        <Route path='/locations/book-home' exact component={BookHome}/>
        <Route path='/locations/book-home/info-cast' component={InfoCast} exact/>
        <Route path='/locations/book-home/info-cast/QRcode' exact component={QRcode}/>
      </LabelContext.Provider>
    </BrowserRouter>
  );
}

export default App;
